- LM2575
    - 'simple switcher'
    - https://cdn-reichelt.de/documents/datenblatt/A200/LM2575-NAT.pdf
- alt/rejected
    - https://www.reichelt.nl/nl/nl/dc-dc-wandler-tba-2-2-w-5-v-200-ma-sil-7-tba-2-1221-p265777.html?&trstct=pol_0&nbc=1
    - https://www.reichelt.nl/nl/nl/staande-inductie-09hvp-ferriet-330-h-l-09hvp-330--p255620.html?&trstct=pol_4&nbc=1
    - https://www.reichelt.nl/nl/nl/elco-radiaal-330-uf-35-v-105-c-nhg-a-330u-35-p200379.html?&trstct=pol_3&nbc=1
    - https://www.reichelt.nl/nl/nl/elco-radiaal-100-uf-16-v-105-c-hd-a-100u-16-p200428.html?&trstct=pol_1&nbc=1
- ok, but the radial THT one has higher current rating https://www.reichelt.nl/nl/nl/smd-powerinductie-1812-330-h-hita-sci1812ft33-p245790.html?&trstct=pos_0&nbc=1

- panasonic aluminum smd elco comparison: https://cdn-reichelt.de/documents/datenblatt/B300/VS_ENG_TDS.pdf

- https://www.reichelt.nl/nl/nl/schakelen-regelaar-step-down-5v-1a-4-75-40vi-to220-5-lm2575t-5g-p255418.html?&nbc=1&trstct=lsbght_sldr::255620
- cin
    - need at least 47 μF, 25V
    - should be aluminum electrolytic, low ESR, can use higher voltage
      rating than necessary to decrease ESR
    - close to reg, short leads
    - for maximum lifetime, RMS ripple current rating of cap. should be
      greater than 1.2 * (5 / 12) * Iload. Iload is 400mA (see inductor). So 200 mA.
        - may not be great at cold temperatures (could use ceramic / tantalum in parallel)
    - from the panasonic ones @ 25V, need 100 μF, so that ripple current is
      close to 200 mA (it's 180)
    - https://www.reichelt.nl/nl/nl/smd-elco-radiaal-100-f-25-v-85-c-2000-h-20--s-v-100u-25-3-p228462.html?&trstct=pol_0&nbc=1
        - +-20% tolerance (so 80 - 120, fine)
        - may not be great at cold temperatures (could use ceramic / tantalum in parallel)
        - diameter = 6.3, height 7.7
- cout
    - for 5v regular, need 8v rating.
    - 100 μF - 470 μF
    - should be aluminum electrolytic, low ESR, can use higher voltage
      rating than necessary to decrease ESR
    - however, too low ESR is also bad (below 0.05Ω)
        - the SMD ones that say 'lage ESR' are .300Ω
    - close to reg, short leads
    - https://www.reichelt.nl/nl/nl/elektrolytische-condensator-smd-330-f-25-v-105-c-lage-esr-8-vf-330-25-k-f-p84772.html?&trstct=pol_5&nbc=1
        - +-20% tolerance (so 260 - 400, fine)
        - may not be great at cold temperatures (could use ceramic / tantalum in parallel)
        - diameter = 8, height = 10.2
- https://www.reichelt.nl/nl/nl/vermogensinductiespoel-radiaal-330-h-bou-rlb0914-331k-p245716.html?&trstct=pol_8&nbc=1
    - listed under SMD but it's THT
    - given current = 1.5 Iload, max Iload is 600 mA / 1.5 = 400 mA
    - it's a power inductor so it's made for this, can operate at 52kHz (and
      much more), presumably made of a material that minimises EMI and other
      undesirable properties
- https://www.reichelt.nl/nl/nl/dil-miniatuurrelais-hjr-4102-5v-1-wisselaar-5a-hjr-4102-l-5v-p101950.html?&trstct=pol_5&nbc=1
- https://www.reichelt.nl/nl/nl/aansluitklem-2-polig-rm-5-08-mm-90--lakl-1-5-2-5-08-p169871.html?search=phoenix+lakl&&r=1
- https://secure.reichelt.nl/nl/nl/leiterplattenklemme-3-polig-rm-2-54-mm-dg308-2-54-3-p276215.html?&trstct=pol_1&nbc=1
- https://www.reichelt.nl/nl/nl/schottkydiode-do41-40v-1a-1n-5819-p41850.html?&trstct=pos_0&nbc=1

https://www.reichelt.nl/nl/nl/ic-socket-8-polig-super-plat-gedraaid-verguld--gs-8p-p8231.html?&trstct=pol_1&nbc=1
