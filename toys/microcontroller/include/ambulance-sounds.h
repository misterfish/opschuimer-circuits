#ifndef _AMBULANCE_SOUNDS_H
#define _AMBULANCE_SOUNDS_H

void ambulance_sounds_init ();
uint16_t *ambulance_sounds_sine ();

#endif
