#ifndef _CONFIG_PROJECT_AMBULANCE_H
#define _CONFIG_PROJECT_AMBULANCE_H

#define CLK_PRESCALE 1
// --- defined in Makefile so that the ws2812 library has the same.
#define F_CPU F_CPU_WS2812
#define SPEAK true
#define SPEAK_LEVEL ACA_SPEAK_DEBUG

#define OUT0_PORT PORTA
#define OUT0_POS 5
#define OUT1_PORT PORTA
#define OUT1_POS 6
#define OUT2_PORT PORTA
#define OUT2_POS 7
#define OUT3_PORT PORTB
#define OUT3_POS 0

#endif
