#ifndef _CONFIG_PROJECT_RFM69_TEST_PRIVATE_H
#define _CONFIG_PROJECT_RFM69_TEST_PRIVATE_H

#define AES_KEY \
  0xc1, 0x51, 0x86, 0xf6, 0x65, 0xc6, 0x2b, 0x9d, \
  0xac, 0xdc, 0x51, 0xf9, 0xd6, 0x2b, 0xc9, 0xb5,

#define ADDRESS_RX 0x13
#define ADDRESS_TX 0x12

/* See rfm69-test for details.
 * A msg of exactly length 63 works.
 */

#define MSG \
  'd', 'i', 't', 'd', 'e', 'u', 'r', 't', 'd', 'e', \
  'd', 'o', 'e', 't', 'd', 'i', 'c', 'h', 't', 'x', \
  'd', 'i', 't', 'd', 'e', 'u', 'r', 't', 'd', 'e', \
  'd', 'o', 'e', 't', 'd', 'i', 'c', 'h', 't', 'x', \
  'd', 'i', 't', 'd', 'e', 'u', 'r', 't', 'd', 'e', \
  'd', 'o', 'e', 't', 'd', 'i', 'c', 'h', 't', 'x', \
  'd', 'o', 'e',

#define MSG_LENGTH 63

#endif
