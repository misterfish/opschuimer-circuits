#ifndef _CONFIG_PROJECT_DOORBELL_H
#define _CONFIG_PROJECT_DOORBELL_H

#include <alleycat-avr.h>

#define MODE_RFM69_TX 1
#define MODE_RFM69_RX 2

#if MODE_RFM69 != MODE_RFM69_TX && MODE_RFM69 != MODE_RFM69_RX
# warning MODE_RFM69 must be either MODE_RFM69_TX or MODE_RFM69_RX
#endif

#define BITRATE_BITS 19200

#define LED0_PORT OUT0_PORT
#define LED0_POS OUT0_POS
#define LED1_PORT OUT1_PORT
#define LED1_POS OUT1_POS
#define LED2_PORT OUT2_PORT
#define LED2_POS OUT2_POS
#define LED3_PORT OUT3_PORT
#define LED3_POS OUT3_POS

#define BTN_0_PORT PORTF
#define BTN_0_POS 2
#define BTN_1_PORT PORTF
#define BTN_1_POS 3
#define BTN_2_PORT PORTF
#define BTN_2_POS 4
// --- F_CPU 1e6 with prescale of 6 works fine.
#define CLK_PRESCALE 1
#define DEBUG true
#define F_CPU 4000000
#define FREQUENCY 915
#define FREQUENCY_DEVIATION 70000
#define RESET_PORT PORTF
#define RESET_POS 5
#define SPEAK_LEVEL ACA_SPEAK_INFO
// #define SPEAK_LEVEL ACA_SPEAK_DEBUG
#define SPEAK true
#define TIMEOUT_MODESET_MS 500
#define TIMEOUT_PACKET_SENT_MS 1000
#define TIMEOUT_PAYLOAD_READY_MS 1000
#define USE_PA_BOOST true // --- for H/HC versions of the module

typedef struct {
  aca_pinout_t led0;
  aca_pinout_t led1;
  aca_pinout_t led2;
  aca_pinout_t led3;
} leds_t;

typedef struct {
  leds_t leds;
  aca_pinout_t enable;
} app_t;

#endif
