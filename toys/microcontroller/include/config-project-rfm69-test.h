#ifndef _CONFIG_PROJECT_RFM69_TEST_H
#define _CONFIG_PROJECT_RFM69_TEST_H

#include <alleycat-avr.h>

/* Input of length d becomes length (d+16)&~15, i.e., d + 16 rounded
 * down to the nearest multiple of 16, after AES encryption. So it would
 * seem this needs to be less than 64.
 * At the moment it only seems to work
 *
 * --- needs to be exactly 63, so that with address byte it's 64.
 */
#define MSG \
  'd', 'i', 't', 'd', 'e', 'u', 'r', 't', 'd', 'e', \
  'd', 'o', 'e', 't', 'd', 'i', 'c', 'h', 't', 'x', \
  'd', 'i', 't', 'd', 'e', 'u', 'r', 't', 'd', 'e', \
  'd', 'o', 'e', 't', 'd', 'i', 'c', 'h', 't', 'x', \
  'd', 'i', 't', 'd', 'e', 'u', 'r', 't', 'd', 'e', \
  'd', 'o', 'e', 't', 'd', 'i', 'c', 'h', 't', 'x', \
  'd', 'o', 'e',
#define MSG_LENGTH 63

#define MODE_RFM69_TX 1
#define MODE_RFM69_RX 2

#if MODE_RFM69 != MODE_RFM69_TX && MODE_RFM69 != MODE_RFM69_RX
# warning MODE_RFM69 must be either MODE_RFM69_TX or MODE_RFM69_RX
#endif

#define ADDRESS_RX 0x01
#define ADDRESS_TX 0x00
#define BITRATE_BITS 19200
#define CLK_PRESCALE 1
#define DEBUG true
#define F_CPU 24000000
#define FREQUENCY 915
#define FREQUENCY_DEVIATION 70000
#define RESET_PORT PORTF
#define RESET_POS 5
#define SPEAK_LEVEL ACA_SPEAK_DEBUG
#define SPEAK true
#define TIMEOUT_MODESET_MS 500
#define TIMEOUT_PACKET_SENT_MS 1000
#define TIMEOUT_PAYLOAD_READY_MS 1000
#define USE_PA_BOOST true // --- for H/HC versions of the module

#define LED0_PORT OUT0_PORT
#define LED0_POS OUT0_POS
#define LED1_PORT OUT1_PORT
#define LED1_POS OUT1_POS
#define LED2_PORT OUT2_PORT
#define LED2_POS OUT2_POS
#define LED3_PORT OUT3_PORT
#define LED3_POS OUT3_POS

typedef struct {
  aca_pinout_t led0;
  aca_pinout_t led1;
  aca_pinout_t led2;
  aca_pinout_t led3;
} leds_t;

typedef struct {
  leds_t leds;
  aca_pinout_t enable;
} app_t;

#endif
