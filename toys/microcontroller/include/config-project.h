#ifndef _CONFIG_PROJECT_H
#define _CONFIG_PROJECT_H

#define NUM_OUTS 4

#define USART_BAUD_RATE 9600

#define OUT0_PORT PORTA
#define OUT0_POS 5
#define OUT1_PORT PORTA
#define OUT1_POS 6
#define OUT2_PORT PORTA
#define OUT2_POS 7
#define OUT3_PORT PORTB
#define OUT3_POS 0

#define BTN0_PORT PORTF
#define BTN0_POS 2
#define BTN1_PORT PORTF
#define BTN1_POS 3
#define BTN2_PORT PORTF
#define BTN2_POS 4
#define BTN3_PORT PORTF
#define BTN3_POS 5

#endif
