#ifndef _LIB_OSCLEDS_H
#define _LIB_OSCLEDS_H

#include <stdint.h>

#include <alleycat-avr.h>

struct osc_speed {
  uint16_t on_ms;
  uint16_t off_ms;
  uint32_t max_cycles;
};

struct osc_speed_cycle {
  struct osc_speed speed;
  struct osc_speed_cycle *nxt;
};

struct state {
  bool onoff;
  struct osc_speed_cycle *cycle;
};

struct osc_led {
  aca_pinout_t led;
  struct state state;
  bool limit_cycles;
  uint32_t cycles_remaining;
};

bool osc_leds_init ();
bool init_osc_led (aca_pinout_t led, struct osc_led *init_osc_led);
uint16_t osc_led_cur_on_ms (struct osc_led *osc_led);

#endif
