#ifndef TWI_H
#define TWI_H 1
enum response_t { R_NONE, ACK, NACK };
typedef bool (*on_any_t)(uint8_t status);
typedef void (*on_error_t)(uint8_t status);
typedef bool (*on_start_t)();
typedef void (*on_stop_t)();
typedef bool (*on_receive_t)(uint8_t data);
typedef bool (*on_request_t)(volatile uint8_t *buf);
bool i2c_target_init (
  uint8_t addr, uint8_t sda_setup_cyc,
  on_any_t _on_any, on_error_t _on_error,
  on_start_t _on_start, on_stop_t _on_stop,
  on_receive_t _on_receive, on_request_t _on_request
);

#endif

