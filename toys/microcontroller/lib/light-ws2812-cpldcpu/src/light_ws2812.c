/*
* light weight WS2812 lib V2.5b
*
* Controls WS2811/WS2812/WS2812B RGB-LEDs
* Author: Tim (cpldcpu@gmail.com)
*
* Jan 18, 2014  v2.0b Initial Version
* Nov 29, 2015  v2.3  Added SK6812RGBW support
* Nov 11, 2023  v2.5  Added support for ports that cannot be addressed with "out"
*                       Added LGT8F88A support
* May 1, 2024   v2.6 Added support for reduced core AVRs
*
* License: GNU GPL v2+ (see License.txt)
*/

#include "light_ws2812.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

// void inline ws2812_setleds_pin(struct cRGB *ledarray, uint16_t leds, uint8_t pinmask)
// {
  // ws2812_sendarray_mask((uint8_t*)ledarray,leds+leds+leds,pinmask);
  // _delay_us(ws2812_resettime);
// }
// 
// Setleds for SK6812RGBW
// void inline ws2812_setleds_rgbw(struct cRGBW *ledarray, uint16_t leds)
// {
  // ws2812_sendarray_mask((uint8_t*)ledarray,leds<<2,_BV(ws2812_pin));
  // _delay_us(ws2812_resettime);
// }

/*
  This routine writes an array of bytes with RGB values to the Dataout pin
  using the fast 800kHz clockless WS2811/2812 protocol.
*/

// Timing in ns
#define w_zeropulse   350
#define w_onepulse    900
#define w_totalperiod 1250

// Fixed cycles used by the inner loop
#if defined(__LGT8F__)     // LGT8F88A
#define w_fixedlow    4
#define w_fixedhigh   6
#define w_fixedtotal  10
// #elif __AVR_ARCH__ == 100  // reduced core AVR
// #define w_fixedlow    2
// #define w_fixedhigh   4
// #define w_fixedtotal  8
#else                      // all others
#define w_fixedlow    3
#define w_fixedhigh   6
#define w_fixedtotal  10
#endif


// Insert NOPs to match the timing, if possible
#define w_zerocycles    (((F_CPU/1000)*w_zeropulse          )/1000000)
#define w_onecycles     (((F_CPU/1000)*w_onepulse    +500000)/1000000)
#define w_totalcycles   (((F_CPU/1000)*w_totalperiod +500000)/1000000)

// w1 - nops between rising edge and falling edge - low
#define w1 (w_zerocycles-w_fixedlow)
// w2   nops between fe low and fe high
#define w2 (w_onecycles-w_fixedhigh-w1)
// w3   nops to complete loop
#define w3 (w_totalcycles-w_fixedtotal-w1-w2)

#if w1>0
  #define w1_nops w1
#else
  #define w1_nops  0
#endif

// The only critical timing parameter is the minimum pulse length of the "0"
// Warn or throw error if this timing can not be met with current F_CPU settings.
#define w_lowtime ((w1_nops+w_fixedlow)*1000000)/(F_CPU/1000)
#if w_lowtime>550
   #error "Light_ws2812: Sorry, the clock speed is too low. Did you set F_CPU correctly?"
#elif w_lowtime>450
   #warning "Light_ws2812: The timing is critical and may only work on WS2812B, not on WS2812(S)."
   #warning "Please consider a higher clockspeed, if possible"
#endif

#if w2>0
#define w2_nops w2
#else
#define w2_nops  0
#endif

#if w3>0
#define w3_nops w3
#else
#define w3_nops  0
#endif

#define w_nop1  "nop      \n\t"
#if (ws2812_interrupt_handling)
#define w_nop2  "brid .+0 \n\t"
#else
#define w_nop2  "brtc .+0 \n\t"
#endif
#define w_nop4  w_nop2 w_nop2
#define w_nop8  w_nop4 w_nop4
#define w_nop16 w_nop8 w_nop8

static void inline ws2812_sendarray_mask_old (uint8_t *data,uint16_t datlen,uint8_t maskhi)
{
  // `maskhi` is 0x80 if P?7 is LED DATA
  uint8_t curbyte,ctr,masklo;
  uint8_t sreg_prev;
// #if __AVR_ARCH__ != 100
  // uint8_t *port = (uint8_t*) _SFR_MEM_ADDR(ws2812_PORTREG);
  uint16_t *port = (uint16_t*) _SFR_MEM_ADDR(ws2812_PORTREG.OUT);
// #endif

  // ws2812_DDRREG |= maskhi; // Enable output

  // `masklo` and `maskhi` are written to PORT? to drive the DATA line low or
  // high (rather than setting or clearing the bit in PORT?)
  // masklo	=~maskhi&ws2812_PORTREG;
  // maskhi |=        ws2812_PORTREG;
  masklo	=~maskhi&ws2812_PORTREG.OUT;
  maskhi |=        ws2812_PORTREG.OUT;

  sreg_prev=SREG;

#if (ws2812_interrupt_handling)
  cli();
#endif

  while (datlen--) {
    curbyte=*data++;

    __asm__ volatile(
    "       ldi   %0,8  \n\t"
#if (ws2812_interrupt_handling)
    "       clt         \n\t"
#endif
    "loop%=:            \n\t"
// #if __AVR_ARCH__ == 100
    // "       out   %2,%3 \n\t"    //  '1' [01] '0' [01] - re
// #else
    "       st    X,%3 \n\t"    //  '1' [02] '0' [02] - re
// #endif

#if (w1_nops&1)
w_nop1
#endif
#if (w1_nops&2)
w_nop2
#endif
#if (w1_nops&4)
w_nop4
#endif
#if (w1_nops&8)
w_nop8
#endif
#if (w1_nops&16)
w_nop16
#endif
#if defined(__LGT8F__)
    "       bst   %1,7  \n\t"    //  '1' [02] '0' [02]
    "       brts  1f    \n\t"    //  '1' [04] '0' [03]
    "       st    X,%4  \n\t"    //  '1' [--] '0' [04] - fe-low
    "1:     lsl   %1    \n\t"    //  '1' [05] '0' [05]
// #elif __AVR_ARCH__ == 100
    // "       sbrs  %1,7  \n\t"    //  '1' [03] '0' [02]
    // "       out   %2,%4 \n\t"    //  '1' [--] '0' [03] - fe-low
    // "       lsl   %1    \n\t"    //  '1' [04] '0' [04]
#else
    "       sbrs  %1,7  \n\t"    //  '1' [04] '0' [03]
    "       st    X,%4 \n\t"     //  '1' [--] '0' [05] - fe-low
    "       lsl   %1    \n\t"    //  '1' [05] '0' [06]
#endif
#if (w2_nops&1)
  w_nop1
#endif
#if (w2_nops&2)
  w_nop2
#endif
#if (w2_nops&4)
  w_nop4
#endif
#if (w2_nops&8)
  w_nop8
#endif
#if (w2_nops&16)
  w_nop16
#endif
// #if __AVR_ARCH__ == 100
    // "       out   %2,%4 \n\t"    //  '1' [+1] '0' [+1] - fe-high
// #else
    "       brcc skipone%= \n\t"    //  '1' [+1] '0' [+2] -
    "       st   X,%4      \n\t"    //  '1' [+3] '0' [--] - fe-high
    "skipone%=:               "     //  '1' [+3] '0' [+2] -
// #endif
#if (w3_nops&1)
w_nop1
#endif
#if (w3_nops&2)
w_nop2
#endif
#if (w3_nops&4)
w_nop4
#endif
#if (w3_nops&8)
w_nop8
#endif
#if (w3_nops&16)
w_nop16
#endif

    "       dec   %0    \n\t"    //  '1' [+4] '0' [+3]
    "       brne  loop%=\n\t"    //  '1' [+5] '0' [+4]
    :	"=&d" (ctr)
// #if __AVR_ARCH__ == 100
    // :	"r" (curbyte), "I" (_SFR_IO_ADDR(ws2812_PORTREG)), "r" (maskhi), "r" (masklo)
// #else
    :	"r" (curbyte), "x" (port), "r" (maskhi), "r" (masklo)
// #endif

    );
  }

  SREG=sreg_prev;

#if (ws2812_interrupt_handling)
  sei();
#endif

}

static void inline ws2812_sendarray_mask_spence (uint8_t *data,uint16_t datlen,uint8_t pinMask)
// void ws2812_sendarray_mask_spence (uint8_t *data,uint16_t datlen,uint8_t pinMask)
{

  // can show etc.
  // _delay_ms (1);

  volatile uint16_t i = datlen;
  volatile uint8_t *ptr = data;
  volatile uint8_t b = *ptr++;
  volatile uint16_t *port = (uint16_t*) _SFR_MEM_ADDR(ws2812_PORTREG.OUT);
  volatile uint8_t hi, lo = 0;
  cli();

  #if (F_CPU >= 7400000UL) && (F_CPU <= 9500000UL)
    volatile uint8_t n1, n2 = 0;

    hi   = *port |  pinMask;
    lo   = *port & ~pinMask;
    n1 = lo;
    if (b & 0x80) n1 = hi;

    asm volatile(
     "_head8:"                    "\n\t" // Clk  Pseudocode
      // Bit 7:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi
      "mov  %[n2]   , %[lo]"      "\n\t" // 1    n2   = lo
      "st   %a[port], %[n1]"      "\n\t" // 1    PORT = n1
      "rjmp .+0"                  "\n\t" // 2    nop nop
      "sbrc %[byte] , 6"          "\n\t" // 1-2  if (b & 0x40)
       "mov %[n2]   , %[hi]"      "\n\t" // 0-1   n2 = hi
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo
      "rjmp .+0"                  "\n\t" // 2    nop nop
      // Bit 6:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi
      "mov  %[n1]   , %[lo]"      "\n\t" // 1    n1   = lo
      "st   %a[port], %[n2]"      "\n\t" // 1    PORT = n2
      "rjmp .+0"                  "\n\t" // 2    nop nop
      "sbrc %[byte] , 5"          "\n\t" // 1-2  if (b & 0x20)
       "mov %[n1]   , %[hi]"      "\n\t" // 0-1   n1 = hi
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo
      "rjmp .+0"                  "\n\t" // 2    nop nop
      // Bit 5:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi
      "mov  %[n2]   , %[lo]"      "\n\t" // 1    n2   = lo
      "st   %a[port], %[n1]"      "\n\t" // 1    PORT = n1
      "rjmp .+0"                  "\n\t" // 2    nop nop
      "sbrc %[byte] , 4"          "\n\t" // 1-2  if (b & 0x10)
       "mov %[n2]   , %[hi]"      "\n\t" // 0-1   n2 = hi
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo
      "rjmp .+0"                  "\n\t" // 2    nop nop
      // Bit 4:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi
      "mov  %[n1]   , %[lo]"      "\n\t" // 1    n1   = lo
      "st   %a[port], %[n2]"      "\n\t" // 1    PORT = n2
      "rjmp .+0"                  "\n\t" // 2    nop nop
      "sbrc %[byte] , 3"          "\n\t" // 1-2  if (b & 0x08)
       "mov %[n1]   , %[hi]"      "\n\t" // 0-1   n1 = hi
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo
      "rjmp .+0"                  "\n\t" // 2    nop nop
      // Bit 3:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi
      "mov  %[n2]   , %[lo]"      "\n\t" // 1    n2   = lo
      "st   %a[port], %[n1]"      "\n\t" // 1    PORT = n1
      "rjmp .+0"                  "\n\t" // 2    nop nop
      "sbrc %[byte] , 2"          "\n\t" // 1-2  if (b & 0x04)
       "mov %[n2]   , %[hi]"      "\n\t" // 0-1   n2 = hi
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo
      "rjmp .+0"                  "\n\t" // 2    nop nop
      // Bit 2:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi
      "mov  %[n1]   , %[lo]"      "\n\t" // 1    n1   = lo
      "st   %a[port], %[n2]"      "\n\t" // 1    PORT = n2
      "rjmp .+0"                  "\n\t" // 2    nop nop
      "sbrc %[byte] , 1"          "\n\t" // 1-2  if (b & 0x02)
       "mov %[n1]   , %[hi]"      "\n\t" // 0-1   n1 = hi
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo
      "rjmp .+0"                  "\n\t" // 2    nop nop
      // Bit 1:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi
      "mov  %[n2]   , %[lo]"      "\n\t" // 1    n2   = lo
      "st   %a[port], %[n1]"      "\n\t" // 1    PORT = n1
      "rjmp .+0"                  "\n\t" // 2    nop nop
      "sbrc %[byte] , 0"          "\n\t" // 1-2  if (b & 0x01)
       "mov %[n2]   , %[hi]"      "\n\t" // 0-1   n2 = hi
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo
      "sbiw %[count], 1"          "\n\t" // 2    i-- (don't act on Z flag yet)
      // Bit 0:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi
      "mov  %[n1]   , %[lo]"      "\n\t" // 1    n1   = lo
      "st   %a[port], %[n2]"      "\n\t" // 1    PORT = n2
      "ld   %[byte] , %a[ptr]+"   "\n\t" // 2    b = *ptr++
      "sbrc %[byte] , 7"          "\n\t" // 1-2  if (b & 0x80)
       "mov %[n1]   , %[hi]"      "\n\t" // 0-1   n1 = hi
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo
      "brne _head8"               "\n"   // 2    while(i) (Z flag set above)
    : [ptr]   "+e" (ptr),
      [byte]  "+r" (b),
      [n1]    "+r" (n1),
      [n2]    "+r" (n2),
      [count] "+w" (i)
    : [port]   "e" (port),
      [hi]     "r" (hi),
      [lo]     "r" (lo));

  #elif (F_CPU >= 9500000UL) && (F_CPU <= 11100000UL)
    /*
    volatile uint8_t n1, n2 = 0;  // First, next bits out

    */
    // 14 instruction clocks per bit: HHHHxxxxLLLLL
    // ST instructions:               ^   ^   ^   (T=0,4,7)
    volatile uint8_t next;

    hi   = *port |  pinMask;
    lo   = *port & ~pinMask;
    next = lo;
    if (b & 0x80) {
      next = hi;
    }

    // Don't "optimize" the OUT calls into the bitTime subroutine;
    // we're exploiting the RCALL and RET as 3- and 4-cycle NOPs!
    asm volatile(
     "_head10:"                   "\n\t" //        (T =  0)
      "st   %a[port], %[hi]"      "\n\t" //        (T =  1)
      "rcall _bitTime10"          "\n\t" // Bit 7  (T = 14)
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime10"          "\n\t" // Bit 6
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime10"          "\n\t" // Bit 5
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime10"          "\n\t" // Bit 4
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime10"          "\n\t" // Bit 3
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime10"          "\n\t" // Bit 2
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime10"          "\n\t" // Bit 1
      // Bit 0:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi    (T =  1)
      "nop"                       "\n\t" // 1    nop          (T =  2)
      "ld   %[byte] , %a[ptr]+"   "\n\t" // 2    b = *ptr++   (T =  4)
      "st   %a[port], %[next]"    "\n\t" // 1    PORT = next  (T =  5)
      "nop"                       "\n\t" // 1    nop          (T =  6)
      "mov  %[next] , %[lo]"      "\n\t" // 1    next = lo    (T =  7)
      "sbrc %[byte] , 7"          "\n\t" // 1-2  if (b & 0x80) (T =  8)
       "mov %[next] , %[hi]"      "\n\t" // 0-1    next = hi  (T =  9)
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo    (T = 10)
      "sbiw %[count], 1"          "\n\t" // 2    i--          (T = 12)
      "brne _head10"              "\n\t" // 2    if (i != 0) -> (next byte) (T = 14)
       "rjmp _done10"             "\n\t"
      "_bitTime10:"               "\n\t" //      nop nop nop     (T =  4)
       "st   %a[port], %[next]"   "\n\t" // 1    PORT = next     (T =  5)
       "mov  %[next], %[lo]"      "\n\t" // 1    next = lo       (T =  6)
       "lsl  %[byte]"             "\n\t" // 1    b <<= 1         (T =  7)
       "sbrc %[byte], 7"          "\n\t" // 1-2  if (b & 0x80)    (T =  8)
        "mov %[next], %[hi]"      "\n\t" // 0-1   next = hi      (T =  9)
       "st   %a[port], %[lo]"     "\n\t" // 1    PORT = lo       (T = 10)
       "ret"                      "\n\t" // 4    return to above where we called from
      "_done10:"                  "\n"
    : [ptr]   "+e" (ptr),
      [byte]  "+r" (b),
      [next]  "+r" (next),
      [count] "+w" (i)
    : [port]   "e" (port),
      [hi]     "r" (hi),
      [lo]     "r" (lo));



// 12 MHz(ish) AVRxt --------------------------------------------------------
#elif (F_CPU >= 11100000UL) && (F_CPU <= 14300000UL)

    // 15 instruction clocks per bit: HHHHxxxxxxLLLLL      H:4 x:6 L5
    // OUT instructions:              ^   ^     ^     (T=0,4,10)

    volatile uint8_t next;

    hi   = *port |  pinMask;
    lo   = *port & ~pinMask;
    next = lo;
    if (b & 0x80) {
      next = hi;
    }

      // Don't "optimize" the OUT calls into the bitTime subroutine;
      // we're exploiting the RCALL and RET as 3- and 4-cycle NOPs!
    asm volatile(
     "_head12:"                   "\n\t" //        (T =  0)
      "st   %a[port], %[hi]"      "\n\t" //        (T =  1)
      "rcall _bitTime12"          "\n\t" // Bit 7  (T = 15)
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime12"          "\n\t" // Bit 6
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime12"          "\n\t" // Bit 5
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime12"          "\n\t" // Bit 4
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime12"          "\n\t" // Bit 3
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime12"          "\n\t" // Bit 2
      "st   %a[port], %[hi]"      "\n\t"
      "rcall _bitTime12"          "\n\t" // Bit 1
      // Bit 0:
      "st   %a[port], %[hi]"      "\n\t" // 1    PORT = hi    (T =  1)
      "nop"                       "\n\t" // 1    nop          (T =  2)
      "ld   %[byte] , %a[ptr]+"   "\n\t" // 2    b = *ptr++   (T =  4)
      "st   %a[port], %[next]"    "\n\t" // 1    PORT = next  (T =  5)
      "mov  %[next] , %[lo]"      "\n\t" // 1    next = lo    (T =  6)
      "sbrc %[byte] , 7"          "\n\t" // 1-2  if (b & 0x80) (T =  7)
       "mov %[next] , %[hi]"      "\n\t" // 0-1    next = hi  (T =  8)
      "rjmp .+0"                  "\n\t" // 2    nop nop      (T = 10)
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo    (T = 11)
      "sbiw %[count], 1"          "\n\t" // 2    i--          (T = 13)
      "brne _head12"              "\n\t" // 2    if (i != 0) -> (next byte)
       "rjmp _done12"             "\n\t"
      "_bitTime12:"               "\n\t" //      nop nop nop     (T =  4)
       "st   %a[port], %[next]"   "\n\t" // 1    PORT = next     (T =  5)
       "mov  %[next], %[lo]"      "\n\t" // 1    next = lo       (T =  6)
       "lsl  %[byte]"             "\n\t" // 1    b <<= 1         (T =  7)
       "sbrc %[byte], 7"          "\n\t" // 1-2  if (b & 0x80)    (T =  8)
        "mov %[next], %[hi]"      "\n\t" // 0-1   next = hi      (T =  9)
       "nop"                      "\n\t" // 1                    (T = 10)
       "st   %a[port], %[lo]"     "\n\t" // 1    PORT = lo       (T = 11)
       "ret"                      "\n\t" // 4    return to above where we called from
      "_done12:"                  "\n"
    : [ptr]   "+e" (ptr),
      [byte]  "+r" (b),
      [next]  "+r" (next),
      [count] "+w" (i)
    : [port]   "e" (port),
      [hi]     "r" (hi),
      [lo]     "r" (lo));


// 16 MHz(ish) AVRxt ------------------------------------------------------
#elif (F_CPU >= 15400000UL) && (F_CPU <= 19000000L)

    // WS2811 and WS2812 have different hi/lo duty cycles; this is
    // similar but NOT an exact copy of the prior 400-on-8 code.

    // 20 inst. clocks per bit: HHHHHxxxxxxxxLLLLLLL      H:5 x:8 L7
    // ST instructions:         ^    ^       ^       (T=0,5,13)

    volatile uint8_t next, bit;

    hi   = *port |  pinMask;
    lo   = *port & ~pinMask;
    next = lo;
    bit  = 8;

    asm volatile(
     "_head16:"                   "\n\t" // Clk  Pseudocode    (T =  0)
      "st   %a[port],  %[hi]"     "\n\t" // 1    PORT = hi     (T =  1)
      "nop"                       "\n\t" // 1    nop           (T =  2)
      "sbrc %[byte],  7"          "\n\t" // 1-2  if (b & 128)
       "mov  %[next], %[hi]"      "\n\t" // 0-1   next = hi    (T =  4)
      "dec  %[bit]"               "\n\t" // 1    bit--         (T =  5)
      "st   %a[port],  %[next]"   "\n\t" // 1    PORT = next   (T =  6)
      "nop"                       "\n\t" // 1    nop           (T =  7)
      "mov  %[next] ,  %[lo]"     "\n\t" // 1    next = lo     (T =  8)
      "breq _nextbyte16"          "\n\t" // 1-2  if (bit == 0) (from dec above)
      "rol  %[byte]"              "\n\t" // 1    b <<= 1       (T = 10)
      "rjmp .+0"                  "\n\t" // 2    nop nop       (T = 12)
      "nop"                       "\n\t" // 1    nop           (T = 13)
      "st   %a[port],  %[lo]"     "\n\t" // 1    PORT = lo     (T = 14)
      "rjmp .+0"                  "\n\t" // 2    nop nop       (T = 16)
      "rjmp .+0"                  "\n\t" // 2    nop nop       (T = 18)
      "rjmp _head16"              "\n\t" // 2    -> _head16 (next bit out) (T=20)
     "_nextbyte16:"                "\n\t" //                    (T = 10)
      "ldi  %[bit]  ,  8"         "\n\t" // 1    bit = 8       (T = 11)
      "ld   %[byte] ,  %a[ptr]+"  "\n\t" // 2    b = *ptr++    (T = 13)
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo     (T = 14)
      "rjmp .+0"                  "\n\t" // 2    nop nop       (T = 16)
      "sbiw %[count], 1"          "\n\t" // 2    i--           (T = 18)
       "brne _head16"             "\n"   // 2    if (i != 0) -> _head16 (bit 0 of next byte.) (T=20)
    : [ptr]   "+e" (ptr),
      [byte]  "+r" (b),
      [bit]   "+d" (bit),
      [next]  "+r" (next),
      [count] "+w" (i)
    : [port]   "e" (port),
      [hi]     "r" (hi),
      [lo]     "r" (lo));

// 20 MHz(ish) AVRxt ------------------------------------------------------
#elif (F_CPU >= 19000000UL) && (F_CPU <= 22000000L)


    // 25 inst. clocks per bit: HHHHHHHxxxxxxxLLLLLLLLLLL      H:7 x:7 L11
    // ST instructions:         ^      ^      ^       (T=0,7,14)

    volatile uint8_t next, bit;

    hi   = *port |  pinMask;
    lo   = *port & ~pinMask;
    next = lo;
    bit  = 8;

    asm volatile(
     "_head20:"                   "\n\t" // Clk  Pseudocode    (T =  0)
      "st   %a[port],  %[hi]"     "\n\t" // 1    PORT = hi     (T =  1)
      "sbrc %[byte],  7"          "\n\t" // 1-2  if (b & 128)
       "mov  %[next], %[hi]"      "\n\t" // 0-1   next = hi    (T =  3)
      "dec  %[bit]"               "\n\t" // 1    bit--         (T =  4)
      "nop"                       "\n\t" // 1    nop           (T =  5)
      "rjmp .+0"                  "\n\t" // 2    nop nop       (T =  7)
      "st   %a[port],  %[next]"   "\n\t" // 1    PORT = next   (T =  8)
      "mov  %[next] ,  %[lo]"     "\n\t" // 1    next = lo     (T =  9)
      "breq _nextbyte20"          "\n\t" // 1-2  if (bit == 0) (from dec above) -> _nextbyte20
      "rol  %[byte]"              "\n\t" // 1    b <<= 1       (T = 11)
      "rjmp .+0"                  "\n\t" // 2    nop nop       (T = 13)
      "nop"                       "\n\t" // 1    nop           (T = 14)
      "st   %a[port],  %[lo]"     "\n\t" // 1    PORT = lo     (T = 15)
      "rcall _onlydelay20"        "\n\t" // 2+4+2 = 7          (T = 23)
      "rjmp _head20"              "\n\t" // 2    -> _head20 (next bit out T = 25)
     "_onlydelay20:"              "\n\t" // 2    rcall         (T = n+2)
      "rjmp .+0"                  "\n\t" // 2    nop nop       (T = n+4)
      "ret"                       "\n\t" // 4    4x nop        (T = n+8)
     "_nextbyte20:"               "\n\t" //                    (T = 11)
      "ldi  %[bit]  ,  8"         "\n\t" // 1    bit = 8       (T = 12)
      "ld   %[byte] ,  %a[ptr]+"  "\n\t" // 2    b = *ptr++    (T = 14)
      "nop"                       "\n\t" // 1    nop           (T = 15)
      "st   %a[port], %[lo]"      "\n\t" // 1    PORT = lo     (T = 16)
      "nop"                       "\n\t" // 1    nop           (T = 17)
      "rjmp .+0"                  "\n\t" // 2    nop nop       (T = 19)
      "rjmp .+0"                  "\n\t" // 2    nop nop       (T = 21)
      "sbiw %[count], 1"          "\n\t" // 2    i--           (T = 23)
       "brne _head20"             "\n"   // 2    if (i != 0) -> _head20 (next byte)
    : [ptr]   "+e" (ptr),
      [byte]  "+r" (b),
      [bit]   "+d" (bit),
      [next]  "+r" (next),
      [count] "+w" (i)
    : [port]   "e" (port),
      [hi]     "r" (hi),
      [lo]     "r" (lo));

#endif

  sei();
}

#if 0
void __attribute__((noinline)) ws2812_sendarray_mask_pololu (uint8_t *colors, uint16_t count)
{
  uint16_t *port = (uint16_t*) _SFR_MEM_ADDR(ws2812_PORTREG.OUT);
  cli();
  while (count--)
  {
    uint8_t portValue = *port;
    asm volatile (
        "ld __tmp_reg__, %a0+\n"
        "ld __tmp_reg__, %a0\n"
        "rcall send_led_strip_byte%=\n"  // Send red component.
        "ld __tmp_reg__, -%a0\n"
        "rcall send_led_strip_byte%=\n"  // Send green component.
        "ld __tmp_reg__, %a0+\n"
        "ld __tmp_reg__, %a0+\n"
        "ld __tmp_reg__, %a0+\n"
        "rcall send_led_strip_byte%=\n"  // Send blue component.
        "rjmp led_strip_asm_end%=\n"     // Jump past the assembly subroutines.

        // send_led_strip_byte subroutine:  Sends a byte to the LED strip.
        "send_led_strip_byte%=:\n"
        "rcall send_led_strip_bit%=\n"  // Send most-significant bit (bit 7).
        "rcall send_led_strip_bit%=\n"
        "rcall send_led_strip_bit%=\n"
        "rcall send_led_strip_bit%=\n"
        "rcall send_led_strip_bit%=\n"
        "rcall send_led_strip_bit%=\n"
        "rcall send_led_strip_bit%=\n"
        "rcall send_led_strip_bit%=\n"  // Send least-significant bit (bit 0).
        "ret\n"

        // send_led_strip_bit subroutine:  Sends single bit to the LED strip by driving the data line
        // high for some time.  The amount of time the line is high depends on whether the bit is 0 or 1,
        // but this function always takes the same time (2 us).
        "send_led_strip_bit%=:\n"
#if F_CPU == 8000000
        "rol __tmp_reg__\n"                      // Rotate left through carry.
#endif
        "sts %2, %4\n"                           // Drive the line high.

#if F_CPU != 8000000
        "rol __tmp_reg__\n"                      // Rotate left through carry.
#endif

#if F_CPU == 16000000
        "nop\n" "nop\n"
#elif F_CPU == 20000000
        "nop\n" "nop\n" "nop\n" "nop\n"
#elif F_CPU != 8000000
#error "Unsupported F_CPU"
#endif

        // If the bit to send is 0, drive the line low now.
        "brcs .+4\n" "sts %2, %3\n"

#if F_CPU == 8000000
        "nop\n" "nop\n"
#elif F_CPU == 16000000
        "nop\n" "nop\n" "nop\n" "nop\n" "nop\n"
#elif F_CPU == 20000000
        "nop\n" "nop\n" "nop\n" "nop\n" "nop\n"
        "nop\n" "nop\n"
#endif

        // If the bit to send is 1, drive the line low now.
        "brcc .+4\n" "sts %2, %3\n"

        "ret\n"
        "led_strip_asm_end%=: "
        : "=b" (colors)
        : "0" (colors),           // %a0 points to the next color to display
          // "" (&PORTB),   // %2 is the port register (e.g. PORTH)
          "" (port),   // %2 is the port register (e.g. PORTH)
          "r" ((uint8_t)(portValue & ~(1 << ws2812_pin))),  // %3
          "r" ((uint8_t)(portValue | (1 << ws2812_pin)))    // %4
    );
  }
  sei ();
  _delay_us (80);
}
#endif

void ws2812_sendarray(uint8_t *data,uint16_t datlen)
{
  // ws2812_sendarray_mask_pololu (data, datlen);
  ws2812_sendarray_mask_spence (data, datlen, _BV (ws2812_pin));
  // ws2812_sendarray_mask_old (data, datlen, _BV (ws2812_pin));
}

// Setleds for standard RGB
// void inline ws2812_setleds(struct cRGB *ledarray, uint16_t leds)
// {
   // ws2812_setleds_pin(ledarray,leds, _BV(ws2812_pin));
// }

