#include <math.h>
#include <stdint.h>

#include "ambulance-sounds.h"

#define SINE_PERIOD_STEPS       (10)
#define SINE_AMPLITUDE          (511)
#define SINE_DC_OFFSET          (512)

// --- seems to be the max possible (3.3v)
// #define VOL_ENVELOPE .1
#define VOL_ENVELOPE 1.0

uint16_t sineWave[SINE_PERIOD_STEPS];

static void sineWaveInit (void) {
  for (uint8_t i = 0; i < SINE_PERIOD_STEPS; i++) {
    sineWave[i] = VOL_ENVELOPE * (SINE_DC_OFFSET + (float) SINE_AMPLITUDE * sin (2.0 * M_PI * i / SINE_PERIOD_STEPS));
  }
}

void ambulance_sounds_init () {
  sineWaveInit ();
}

uint16_t *ambulance_sounds_sine () {
  return sineWave;
}
