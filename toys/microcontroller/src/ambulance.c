#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

#include "config-project.h"
#include "config-project-ambulance.h"
#include <util/delay.h>

#include <Light_WS2812/light_ws2812.h>

#include <alleycat-avr/bits.h>
#include <alleycat-avr.h>

#include "ambulance-sounds.h"

#define dbg(...) do { \
  aca_debug (app.printer, ##__VA_ARGS__); \
} while (0)

#define inf(...) do { \
  aca_info (app.printer, ##__VA_ARGS__); \
} while (0)

#define err(...) do { \
  aca_error (app.printer, ##__VA_ARGS__); \
} while (0)

#define ierr(...) do { \
  aca_ierror (app.printer, ##__VA_ARGS__); \
} while (0)

#define die(...) do { \
  aca_die (app.printer, ##__VA_ARGS__); \
} while (0)

#define VREF_STARTUP_TIME 50

#define NUM_SIREN_CYCLES 4
#define SINE_PERIOD_STEPS       (10)
#define FREQ1                   (440)
#define FREQ2                   (1000)
#define delay_us_for_freq(freq, octave_adjust) (((float) F_CPU / octave_adjust / freq) / SINE_PERIOD_STEPS)

#define DEBOUNCE_MS 200
#define NUM_WS_LEDS 8
#define MS_PER_TICK 3

uint64_t ticks_to_ms (uint64_t ticks) {
  return ticks * MS_PER_TICK;
}

uint64_t ms_to_tick (uint64_t ms) {
  return ms * 1.0 / MS_PER_TICK;
}

volatile bool sleeping = false;

struct ws_state {
  uint8_t type;
  bool flip;
  bool dir;
  bool enough;
  uint16_t num_updates;
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t pos;
  uint8_t prev_pos;
  uint8_t max;
  uint8_t s;
};

struct buttons {
  aca_pinin_t button0;
  aca_pinin_t button1;
  aca_pinin_t button2;
  aca_pinin_t button3;
};

struct debounce {
  bool first;
  uint64_t last_ms;
  uint16_t debounce_ms;
};

struct debouncer {
  struct debounce debounce0;
  struct debounce debounce1;
  struct debounce debounce2;
  struct debounce debounce3;
};

struct app {
  aca_printer_t printer;
  aca_loop_t loop;
  aca_pinout_t lamp_led;
  // --- the light_ws2812 data structure
  struct cRGB ws_leds[NUM_WS_LEDS];
  struct ws_state ws;
  struct buttons buttons;
  struct debouncer debouncer;
} app;

static uint16_t *wave;

static bool init_loop (aca_loop_t *loop) {
  uint8_t reasonn = 255;
  char reason[reasonn];
  *loop = aca_loop_mk_loop (8, ACA_TIMER_TCA0_16, MS_PER_TICK, reasonn, reason);
  if (loop == NULL) {
    err ("init_loop (): %s", reason);
    return false;
  }
  return true;
}

void debounce_init (struct debounce *debounce, uint16_t debounce_ms) {
  *debounce = (struct debounce) {
    .first = true,
    .last_ms = 0,
    .debounce_ms = debounce_ms,
  };
}

void init_debounce (struct debouncer *debouncer) {
  debounce_init (&debouncer->debounce0, DEBOUNCE_MS);
  debounce_init (&debouncer->debounce1, DEBOUNCE_MS);
  debounce_init (&debouncer->debounce2, DEBOUNCE_MS);
  debounce_init (&debouncer->debounce3, DEBOUNCE_MS);
}

static void init_printer (aca_printer_t *printer, uint8_t prescale) {
  if (!SPEAK) {
    *printer = aca_mk_printer_null ();
    return;
  }
  aca_speak_set_level (SPEAK_LEVEL);
  aca_usart_t usart = aca_usart_init_default (ACA_USART_CONFIG_USART1, USART_BAUD_RATE);
  *printer = aca_mk_printer_usart (usart);
}

bool debounce_check (struct debounce *debounce, uint64_t cur_ms) {
  uint64_t last_ms = debounce->last_ms;
  bool ok;
  if (debounce->first) {
    debounce->first = false;
    ok = true;
  }
  else ok = cur_ms - last_ms >= debounce->debounce_ms;
  if (ok) debounce->last_ms = cur_ms;
  return ok;
}

bool debounce_and_check_poll (uint64_t ms, struct debounce *debounce, uint8_t pin) {
  if ((PORTF.IN & (1 << pin)) != 0) return false;
  return debounce_check (debounce, ms);
}

static void ws_pin_init (PORT_t *port, uint8_t pos) {
  aca_mk_pinout (port, pos, false);
}

bool button (uint8_t n) {
  dbg ("button %d", n);
  return true;
}

static void ws_init (struct ws_state *ws) {
  ws->flip = false;
  ws->dir = true;
  ws->r = 1;
  ws->g = 1;
  ws->b = 1;
  ws->pos = 0;
  ws->prev_pos = 255;
  ws->max = 100;
  ws->s = 5;
  ws->enough = false;
  ws->num_updates = 0;
}

static void ws_off (struct cRGB *ws_leds) {
  memset (ws_leds, 0, NUM_WS_LEDS * sizeof ws_leds[0]);
  if (false)
  for (uint8_t i = 0; i < NUM_WS_LEDS; i++) {
    ws_leds[i].r = 0;
    ws_leds[i].g = 0;
    ws_leds[i].b = 0;
  }
  ws2812_sendarray ((uint8_t *) ws_leds, NUM_WS_LEDS * 3);
}

static void ws_do_1 (struct ws_state *ws, struct cRGB *ws_leds) {
  memset (ws_leds, 0, NUM_WS_LEDS * sizeof ws_leds[0]);
  if (ws->flip) {
    for (uint8_t i = 0; i < 3; i++) {
      ws_leds [i].r = 255;
      ws_leds [i].g = 0;
      ws_leds [i].b = 0;
    }
  }
  else {
    for (uint8_t i = 5; i < 8; i++) {
      ws_leds [i].r = 0;
      ws_leds [i].g = 0;
      ws_leds [i].b = 255;
    }
  }
  ws2812_sendarray ((uint8_t *) ws_leds, NUM_WS_LEDS * 3);
  ws->flip = !ws->flip;
  ws->num_updates++;
  ws->enough = ws->num_updates == 10;
}

typedef void (*ws_do_t) (struct ws_state *, struct cRGB *);

static bool ws_cb (aca_loop_timeout_t timeout, void *data, ws_do_t f) {
  struct ws_state *ws = &app.ws;
  struct cRGB *ws_leds = app.ws_leds;
  f (ws, ws_leds);
  if (ws->enough) {
    dbg ("enough!");
    ws_init (ws);
    ws_off (ws_leds);
    app.ws.type = 0;
    return false;
  }
  return true;
}

static void ws_do_2 (struct ws_state *ws, struct cRGB *ws_leds) {
  ws_leds [ws->pos].r = ws->r;
  ws_leds [ws->pos].g = ws->g;
  ws_leds [ws->pos].b = ws->b;
  if (ws->prev_pos != 255) {
    ws_leds [ws->prev_pos].r = 0;
    ws_leds [ws->prev_pos].g = 0;
    ws_leds [ws->prev_pos].b = 0;
  }
  ws2812_sendarray ((uint8_t *) ws_leds, NUM_WS_LEDS * 3);
  ws->prev_pos = ws->pos;
  ws->pos = (ws->pos + 1) % NUM_WS_LEDS;
  ws->r = ((ws->r + 1) * ws->s) % ws->max;
  ws->g = ((ws->g + 2) * ws->s) % ws->max;
  ws->b = ((ws->b + 3) * ws->s) % ws->max;
  ws->num_updates++;
  ws->enough = ws->num_updates == 10;
}

static bool ws1_cb (aca_loop_timeout_t timeout, void *data) {
  return ws_cb (timeout, data, ws_do_1);
}

static bool ws2_cb (aca_loop_timeout_t timeout, void *data) {
  return ws_cb (timeout, data, ws_do_2);
}

static void ws_cycle (struct ws_state *ws) {
  ws_init (ws);
  ws->type = (ws->type + 1) % 3;

  aca_loop_clear_timeouts (app.loop);

  if (app.ws.type == 0) {
    ws_off (app.ws_leds);
    return;
  }
  aca_loop_cb_t cb;
  if (app.ws.type == 1) cb = ws1_cb;
  else if (app.ws.type == 2) cb = ws2_cb;
  // --- shouldn't happen
  else {
    ierr ("line %d: invalid", __LINE__);
    return;
  }
  if (!aca_loop_timeout_add (app.loop, cb, NULL, 400, NULL))
    err ("aca_loop_add_timeout failed!");
}

static void DAC0_set_val (uint16_t val) {
  DAC0.DATAL = (val & 0x03) << 6;
  DAC0.DATAH = val >> 2;
}

static void do_wave (uint8_t n) {
  static uint8_t idx = 0;
  DAC0_set_val (wave[idx]);
  idx = (idx + 1) % SINE_PERIOD_STEPS;
  if (n == 1) _delay_us (delay_us_for_freq (FREQ1, 8));
  else if (n == 2) _delay_us (delay_us_for_freq (FREQ2, 8));
}

static void play_siren (uint8_t num_cycles) {
  for (uint8_t i = 0; i < num_cycles; i++) {
    uint32_t m = 2000;
    uint32_t n = (float) m * FREQ2 / FREQ1;
    for (int i = 0; i < m; i++)
      do_wave (1);
    for (int i = 0; i < n; i++)
      do_wave (2);
  }
  DAC0_set_val (0);
}

static void button0 (struct ws_state *ws) {
  dbg ("button 0, playing siren");
  play_siren (NUM_SIREN_CYCLES);
}

static void button1 (struct ws_state *ws) {
  dbg ("button 1");
}

static void button2 (struct ws_state *ws) {
  dbg ("button 2");
  ws_cycle (ws);
}

static void button3 (struct ws_state *ws) {
  dbg ("button 3");
}

static void poll_buttons (aca_loop_t loop, struct debouncer *debouncer, struct ws_state *ws) {
  uint64_t ms = ticks_to_ms (aca_loop_get_tick (loop));
  if (debounce_and_check_poll (ms, &debouncer->debounce0, 2)) button0 (ws);
  if (debounce_and_check_poll (ms, &debouncer->debounce1, 3)) button1 (ws);
  if (debounce_and_check_poll (ms, &debouncer->debounce2, 4)) button2 (ws);
  if (debounce_and_check_poll (ms, &debouncer->debounce3, 5)) button3 (ws);
}

static void ws_leds_send (struct cRGB *ws_leds) {
  ws2812_sendarray ((uint8_t *) ws_leds, NUM_WS_LEDS * 3);
}

static void ws_leds_init (struct cRGB *ws_leds) {
  for (uint8_t i = 0; i < NUM_WS_LEDS; i++) {
    ws_leds [i].r = 0;
    ws_leds [i].g = 0;
    ws_leds [i].b = 0;
  }
  ws_leds_send (ws_leds);
}

static void init_inputs (struct buttons *buttons) {
  aca_pinin_t p0 = aca_mk_pinin (&BTN0_PORT, BTN0_POS, true);
  aca_pinin_t p1 = aca_mk_pinin (&BTN1_PORT, BTN1_POS, true);
  aca_pinin_t p2 = aca_mk_pinin (&BTN2_PORT, BTN2_POS, true);
  aca_pinin_t p3 = aca_mk_pinin (&BTN3_PORT, BTN3_POS, true);
  write (aca_pinin_ctl (p0), PORT_ISC_BOTHEDGES_gc, 0x7);
  write (aca_pinin_ctl (p1), PORT_ISC_BOTHEDGES_gc, 0x7);
  write (aca_pinin_ctl (p2), PORT_ISC_BOTHEDGES_gc, 0x7);
  write (aca_pinin_ctl (p3), PORT_ISC_BOTHEDGES_gc, 0x7);
  buttons->button0 = p0;
  buttons->button1 = p1;
  buttons->button2 = p2;
  buttons->button3 = p3;
}

static void init_sounds () {
  ambulance_sounds_init ();
  wave = ambulance_sounds_sine ();
}

static void init_dac () {
  VREF.DAC0REF = VREF_REFSEL_2V048_gc | VREF_ALWAYSON_bm;
  _delay_us (VREF_STARTUP_TIME);
  DAC0.CTRLA = DAC_OUTEN_bm | DAC_ENABLE_bm;
  DAC0.CTRLA |= DAC_RUNSTDBY_bm;
}

static volatile void timer_tick (float ms) {
  aca_loop_tick (app.loop);
}

int main (void) {
  cli ();
  aca_clock_main_internal_set_freq (F_CPU);
  aca_clock_main_set_prescale (CLK_PRESCALE);
  // --- seems the USART needs a short delay to stabilize.
  if (SPEAK) aca_delay_ms_basic (500);
  init_printer (&app.printer, CLK_PRESCALE);
  init_inputs (&app.buttons);
  init_debounce (&app.debouncer);
  if (!init_loop (&app.loop))
    return 1;
  app.lamp_led = aca_mk_pinout (&OUT0_PORT, OUT0_POS, true);
  init_dac ();
  init_sounds ();
  ws_init (&app.ws);
  // --- hard-coded in light lib
  ws_pin_init (&PORTA, 7);
  ws_leds_init (app.ws_leds);
  sei ();

  set_sleep_mode (SLEEP_MODE_PWR_DOWN);

  inf ("ready");

  uint64_t prev = 0;
  while (true) {
    uint64_t cur = aca_loop_get_tick (app.loop);
    if (prev == cur) continue;
    poll_buttons (app.loop, &app.debouncer, &app.ws);
    aca_loop_do_timeouts (app.loop);
    prev = cur;

#if 0
    if (ws.type == 0 && !loop_has_active_timeout (&loop)) {
      // --- because the ticks get paused, we need to clear the values for
      // `last_tick` in the debounce state, or else the buttons will
      // fail for a very long time.
      debounce_clear ();
      sleep_mode ();
    }
#endif
  }

  return 1;
}

ISR (BADISR_vect) {
}

ISR (PORTF_PORT_vect) {
  PORTF.INTFLAGS = 0xFF;
}

ISR (TCA0_OVF_vect) {
  timer_tick (MS_PER_TICK);
  TCA0.SINGLE.INTFLAGS |= TCA_SINGLE_OVF_bm;
}
