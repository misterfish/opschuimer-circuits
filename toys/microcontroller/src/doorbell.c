#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config-project.h"
#include "config-project-doorbell.h"
#include "config-project-doorbell-private.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include "dump.h"

#include <alleycat-avr.h>
#include <alleycat-avr/bits.h>

#define dbg(...) do { \
  aca_debug (app.printer, ##__VA_ARGS__); \
} while (0)

#define inf(...) do { \
  aca_info (app.printer, ##__VA_ARGS__); \
} while (0)

#define err(...) do { \
  aca_error (app.printer, ##__VA_ARGS__); \
} while (0)

#define die(...) do { \
  aca_die (app.printer, ##__VA_ARGS__); \
} while (0)

#define try_rf(f, args) do { \
  if (!(f args)) return false; \
} while (0)

#define try_r1(f, args) do { \
  if (!(f args)) return 1; \
} while (0)

#define dbg_dump_u8s_u8(...) do { \
  dump_u8s_u8 (debug, ##__VA_ARGS__); \
} while (0)

#define dbg_dump_u8s_char(...) do { \
  dump_u8s_char (debug, ##__VA_ARGS__); \
} while (0)

typedef void (*on_tx_send_t) ();
typedef void (*on_tx_sleep_t) ();
typedef void (*on_rx_recv_t) ();
typedef void (*on_rx_sleep_t) ();

struct led {
  aca_pinout_t led0;
  aca_pinout_t led1;
  aca_pinout_t led2;
  aca_pinout_t led3;
};

struct app {
  aca_printer_t printer;
  struct led leds;
} app;

// --- 0 = tx, 1 = rx
enum mode {
  MODE_RX,
  MODE_TX,
};

uint8_t msg[] = { MSG };
volatile bool button0 = false;
volatile bool button1 = false;

void debug (const char *s) {
  aca_debug (app.printer, "%s", s);
}

float clk_rate () {
  return F_CPU / CLK_PRESCALE;
}

static void init_printer (aca_printer_t *printer, uint8_t prescale) {
  if (!SPEAK) {
    *printer = aca_mk_printer_null ();
    return;
  }
  aca_speak_set_level (SPEAK_LEVEL);
  aca_usart_t usart = aca_usart_init_default (ACA_USART_CONFIG_USART1, USART_BAUD_RATE);
  *printer = aca_mk_printer_usart (usart);
}

static void init_leds (struct led *leds) {
  leds->led0 = aca_mk_pinout (&LED0_PORT, LED0_POS, false);
  leds->led1 = aca_mk_pinout (&LED1_PORT, LED1_POS, false);
  leds->led2 = aca_mk_pinout (&LED2_PORT, LED2_POS, false);
  leds->led3 = aca_mk_pinout (&LED3_PORT, LED3_POS, false);
}

static bool rfm69_tx (aca_rfm69_t rfm, uint8_t tx_address, uint8_t rx_address, on_tx_send_t on_send, on_tx_sleep_t on_sleep) {
  try_rf (aca_rfm69_node_address_set, (rfm, tx_address));

  uint8_t msg_length = MSG_LENGTH;
  uint8_t payload_length = msg_length + 1;
  uint8_t payload[payload_length];
  payload[0] = rx_address;
  memcpy (payload + 1, msg, msg_length);

  // --- fill while sleeping
  try_rf (aca_rfm69_fifo_write, (rfm, payload_length, payload));

  bool timed_out_mode_set;
  try_rf (aca_rfm69_op_mode_set, (rfm, ACA_RFM69_OP_MODE_TX, TIMEOUT_MODESET_MS, clk_rate (), &timed_out_mode_set));
  if (timed_out_mode_set) {
    err ("timeout waiting for ModeReady");
    return false;
  }
  bool timed_out_packet;
  try_rf (aca_rfm69_wait_packet_sent, (rfm, TIMEOUT_PACKET_SENT_MS, clk_rate (), &timed_out_packet));
  if (timed_out_packet) {
    err ("timeout waiting for PacketSent");
    return false;
  }
  inf ("packet of length %d (before encryption) sent", payload_length);

  if (on_send != NULL) on_send ();

  dbg_dump_u8s_u8 (msg_length, msg);
  dbg ("");
  dbg_dump_u8s_char (msg_length, msg);
  dbg ("");

  timed_out_mode_set = false;
  try_rf (aca_rfm69_op_mode_set, (rfm, ACA_RFM69_OP_MODE_SLEEP, TIMEOUT_MODESET_MS, clk_rate (), &timed_out_mode_set));
  if (timed_out_mode_set) {
    err ("timeout waiting for ModeReady");
    return false;
  }
  if (on_sleep != NULL) on_sleep ();
  return true;
}

static bool rfm69_rx (aca_rfm69_t rfm, uint8_t rx_address, on_rx_recv_t on_recv_any, on_rx_recv_t on_recv_match, on_rx_sleep_t on_sleep) {
  uint8_t msg_length = MSG_LENGTH;
  uint8_t payload_length = msg_length + 1;
  if (payload_length > 66) {
    err ("payload_length invalid, aborting (%d > 66)", payload_length);
    return false;
  }

  try_rf (aca_rfm69_node_address_set, (rfm, rx_address));
  try_rf (aca_rfm69_payload_length_set, (rfm, MSG_LENGTH + 1));

  bool timed_out_mode_set;
  try_rf (aca_rfm69_op_mode_set, (rfm, ACA_RFM69_OP_MODE_RX, TIMEOUT_MODESET_MS, clk_rate (), &timed_out_mode_set));
  if (timed_out_mode_set) {
    err ("timeout waiting for ModeReady");
    return false;
  }

  while (true) {
    float rssi;
    try_rf (aca_rfm69_last_rssi_get, (rfm, &rssi));

    bool timed_out_payload = false;
    try_rf (aca_rfm69_wait_payload_ready, (rfm, TIMEOUT_PAYLOAD_READY_MS, clk_rate (), &timed_out_payload));
    if (timed_out_payload) {
      inf ("timeout waiting for PayloadReady");
      continue;
    }
	if (on_recv_any != NULL) on_recv_any ();

    uint8_t payload[payload_length];

    try_rf (aca_rfm69_fifo_read, (rfm, payload_length, payload));
    uint8_t addr = payload[0];
    uint8_t *msg_recv = payload + 1;

    inf ("----");
    inf ("packet received! (address=0x%02x, payload length=%d, last rssi=%d dBm)", addr, msg_length, (int8_t) rssi);

    if (!memcmp (msg, msg_recv, msg_length)) {
      inf ("message matches!");
      if (on_recv_match != NULL) on_recv_match ();
    }

    dbg ("payload:");
    dbg_dump_u8s_u8 ((uint8_t) msg_length, msg_recv);
    dbg_dump_u8s_char ((uint8_t) msg_length, msg_recv);
    dbg ("");

    if (on_sleep != NULL) on_sleep ();
    try_rf (aca_delay_ms_basic, (250));
  }
  return true;
}

void on_tx_send () {
  aca_pin_on (app.leds.led1);
}

void on_tx_sleep () {
  aca_delay_ms_basic_explicit_clock (250, clk_rate ());
  aca_pin_off (app.leds.led1);
}

void on_rx_recv_any () {
  aca_pin_on (app.leds.led1);
}

void on_rx_recv_match () {
  aca_pin_on (app.leds.led2);
}

void on_rx_sleep () {
  aca_delay_ms_basic_explicit_clock (250, clk_rate ());
  aca_pin_off (app.leds.led1);
  aca_pin_off (app.leds.led2);
}

bool info_stats (aca_rfm69_t rfm) {
  char stats[4][255];
  try_rf (aca_rfm69_get_stats, (rfm, 255, &stats));
  for (uint8_t i = 0; i < 4; i++)
    inf (stats [i]);
  return true;
}

void init_buttons () {
  aca_pinin_t b0 = aca_mk_pinin (&BTN_0_PORT, BTN_0_POS, true);
  write (aca_pinin_ctl (b0), PORT_ISC_BOTHEDGES_gc, 0x7);
  aca_pinin_t b1 = aca_mk_pinin (&BTN_1_PORT, BTN_1_POS, true);
  write (aca_pinin_ctl (b1), PORT_ISC_BOTHEDGES_gc, 0x7);
  aca_pinin_t b2 = aca_mk_pinin (&BTN_2_PORT, BTN_2_POS, true);
  write (aca_pinin_ctl (b2), PORT_ISC_BOTHEDGES_gc, 0x7);
  // --- button 3 is used as the reset pin for the rfm69 on some boards
}

bool rfm69_init (aca_spi_t spi, aca_rfm69_t *rfm_ret, aca_printer_t printer) {
  aca_rfm69_t rfm = aca_mk_rfm69 (spi, 20, ACA_RFM69_OCP_TRIM_DEFAULT, printer);
  if (rfm == NULL) return false;
  *rfm_ret = rfm;

  try_rf (aca_rfm69_init, (rfm, &RESET_PORT, RESET_POS, clk_rate (), true));
  try_rf (aca_rfm69_freq_set, (rfm, FREQUENCY));
  try_rf (aca_rfm69_freqdev_set, (rfm, FREQUENCY_DEVIATION));
  try_rf (aca_rfm69_bitrate_set, (rfm, BITRATE_BITS));
  uint8_t aes_key[16] = { AES_KEY };
  try_rf (aca_rfm69_aes_set_key, (rfm, 16, aes_key));
  try_rf (aca_rfm69_aes_enable, (rfm));
  try_rf (info_stats, (rfm));

  return true;
}

int main (void) {
  cli ();
  aca_clock_main_internal_set_freq (F_CPU);
  aca_clock_main_set_prescale (CLK_PRESCALE);
  init_printer (&app.printer, CLK_PRESCALE);
  init_leds (&app.leds);
  init_buttons ();
  sei ();

  // set_sleep_mode (SLEEP_MODE_PWR_DOWN);

  aca_spi_t spi = aca_spi_init (
    ACA_SPI_CONFIG_SPI0_ALT1,
    SPI_MASTER_bm,
    SPI_SSD_bm
  );
  if (spi == NULL) return 1;

  try_r1 (aca_delay_ms_basic, (1000));

  aca_rfm69_t rfm;
  inf ("init");
  if (!rfm69_init (spi, &rfm, app.printer)) {
    dbg ("init failed");
    while (true) {
      aca_pin_toggle (app.leds.led0);
      try_r1 (aca_delay_ms_basic, (1000));
    }
    return 1;
  }

  // --- set length during rx
  try_rf (aca_rfm69_packet_format_fixed, (rfm));

  try_rf (aca_rfm69_power_level_set, (rfm, 20, USE_PA_BOOST));
  // rfm69_mode_set(&rfm, RFM69_OP_MODE_SLEEP);

  inf ("ready!");
  aca_pin_on (app.leds.led0);

  while (true) {
    if (MODE_RFM69 == MODE_RFM69_RX && !rfm69_rx (rfm, ADDRESS_RX, on_rx_recv_any, on_rx_recv_match, on_rx_sleep))
      dbg ("RX failed");
    else if (MODE_RFM69 == MODE_RFM69_TX) {
      if (button0 || button1) {
        cli ();
        dbg ("button %d", button0 ? 0 : 1);
        inf ("ding dong");
        button0 = false;
        button1 = false;
        if (!rfm69_tx (rfm, ADDRESS_TX, ADDRESS_RX, on_tx_send, on_tx_sleep))
          dbg ("TX failed");
        // --- debounce etc, in case tx failed fase.
        try_r1 (aca_delay_ms_basic, (100));
        sei ();
      }
    }
  }

  return 1;
}

ISR(BADISR_vect) {
}

ISR(PORTF_PORT_vect) {
  button0 = is_clr (&BTN_0_PORT.IN, BTN_0_POS);
  button1 = is_clr (&BTN_1_PORT.IN, BTN_1_POS);
  PORTF.INTFLAGS = 0xFF;
}
