#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "dump.h"

#define CBEGIN '|'
#define CCONTINUE CBEGIN
#define CEND '|'
#define CENDLAST '|'

static void _dump_u8s (uint8_t token_size, const char *tmpl, size_t n, uint8_t *buf, print_t print) {
  uint8_t row_length = 10;
  size_t row = 0;
  while (row * row_length < n) {
    uint8_t len = row_length * 4 + row_length - 1 + 1;
    char row_str[len+2];
    char *cur = row_str;
    if (row == 0) *cur++ = CBEGIN;
    else *cur++ = CCONTINUE;
    bool done = false;
    for (uint8_t i = 0; i < row_length; i++) {
      size_t m = row*row_length + i;
      if (m == n) {
        done = true;
        break;
      }
      if (i != 0) *cur++ = ' ';
      snprintf (cur, token_size + 1, tmpl, buf[m]);
      cur += token_size;
    }
    if (done) *cur++ = CENDLAST;
    else *cur++ = CEND;
    *cur++ = '\0';
    print (row_str);
    row += 1;
  }
}


void dump_u8s_u8 (print_t print, size_t n, uint8_t *buf) {
  return _dump_u8s (4, "0x%02x", n, buf, print);
}

void dump_u8s_char (print_t print, size_t n, uint8_t *buf) {
  return _dump_u8s (1, "%c", n, buf, print);
}
