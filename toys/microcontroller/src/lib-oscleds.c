#include <stdbool.h>
#include <stdint.h>

#include "config-project.h"
#include "config-project-oscleds.h"

#include <alleycat-avr.h>

#include "lib-oscleds.h"

// --- determined experimentally. Do this at the very end, after the program
// is finished.
#define xticks_per_sec_1M 2000
#define xms_to_tick(ms) ((uint64_t) (1.0 * ms / 1000 / 1000000 * F_CPU * xticks_per_sec_1M))
// --- stop a single state from flashing after this time
// --- set to 0 for infinite
#define msecs_max 60000

struct osc_speed_cycle osc_speed_cycles [5];

static struct osc_speed_cycle init_osc_speed_cycle (uint16_t on_ms, uint16_t off_ms) {
  uint32_t max_cycles;
  if (on_ms + off_ms == 0) max_cycles = 0;
  else max_cycles = xms_to_tick (msecs_max) * 1.0 / (on_ms + off_ms) * 2;
  struct osc_speed_cycle ret = {
    .speed = {
      .on_ms = on_ms,
      .off_ms = off_ms,
      .max_cycles = max_cycles,
    },
    .nxt = NULL,
  };
  return ret;
}

static void init_osc_speed_cycles () {
  osc_speed_cycles [0] = init_osc_speed_cycle (0, 0);
  osc_speed_cycles [1] = init_osc_speed_cycle (1000, 1000);
  osc_speed_cycles [2] = init_osc_speed_cycle (500, 500);
  osc_speed_cycles [3] = init_osc_speed_cycle (250, 250);
  osc_speed_cycles [4] = init_osc_speed_cycle (70, 70);

  osc_speed_cycles [0].nxt = &osc_speed_cycles [1];
  osc_speed_cycles [1].nxt = &osc_speed_cycles [2];
  osc_speed_cycles [2].nxt = &osc_speed_cycles [3];
  osc_speed_cycles [3].nxt = &osc_speed_cycles [4];
  osc_speed_cycles [4].nxt = &osc_speed_cycles [5];
};

static void osc_speed_cycle_next (struct osc_speed_cycle *cycle) {
  cycle->speed = cycle->nxt->speed;
  cycle->nxt = cycle->nxt->nxt;
}

/* static */ void state_next (struct state *state) {
  struct osc_speed_cycle *cycle = state->cycle;
  osc_speed_cycle_next (cycle);
  state->onoff = state->cycle->speed.on_ms != 0;
}

bool osc_leds_init () {
  init_osc_speed_cycles ();
  return true;
}

bool init_osc_led (aca_pinout_t led, struct osc_led *init_osc_led) {
  struct osc_speed_cycle *cycle = &osc_speed_cycles [0];
  *init_osc_led = (struct osc_led) {
    .led = led,
    .state = {
      .onoff = false,
      .cycle = cycle,
    },
    // --- unknown
    .cycles_remaining = 0,
    .limit_cycles = false,
  };
  return true;
}

uint16_t osc_led_cur_on_ms (struct osc_led *osc_led) {
  return osc_led->state.cycle->speed.on_ms;
}
