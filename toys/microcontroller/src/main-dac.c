#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "config-project.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include "debug.h"
#include "util-avrdx.h"
#include "util-delay.h"
#include "util-general.h"

#define PI 3.141

#define VREF_STARTUP_TIME 50
#define tc_period(ms, prescale) ((uint16_t) (ms / 1000.0 / (float) prescale * F_CPU))

#define debug(...) do { \
  if (DEBUG) debug_say_no_delay (0, ##__VA_ARGS__); \
} while (0)

#define debug_it_nonl(fmt, ...) do { \
  char s[100]; \
  sprintf (s, fmt, ##__VA_ARGS__); \
  if (DEBUG) debug_nonl (s); \
} while (0)

#define debug_it(fmt, ...) do { \
  char s[100]; \
  sprintf (s, fmt, ##__VA_ARGS__); \
  debug (s); \
} while (0)

#define debug_nonl(...) do { \
  if (DEBUG) debug_say_no_delay_nonl (0, ##__VA_ARGS__); \
} while (0)

#define usart_baud_rate 9600

#define rate 44000
#define freq 440

#define SINE_PERIOD_STEPS       (10)
#define SINE_AMPLITUDE          (511)
#define SINE_DC_OFFSET          (512)

#define freq1                   (1760)
#define freq2                   (1000)

#define delay_1_us               (((float) 1e6 / freq1) / SINE_PERIOD_STEPS)
#define delay_2_us               (((float) 1e6 / freq2) / SINE_PERIOD_STEPS)

#define DO_SCALE_TEST false
#define scale_mult 2
#define scale_freq1                   (440 * scale_mult)
#define scale_freq2                   (494 * scale_mult)
#define scale_freq3                   (523 * scale_mult)
#define scale_freq4                   (587 * scale_mult)
#define scale_freq5                   (659 * scale_mult)
#define scale_freq6                   (698 * scale_mult)
#define scale_freq7                   (784 * scale_mult)
#define scale_freq8                   (880 * scale_mult)

#define scale_delay_1_us         (((float) 1e6 / scale_freq1) / SINE_PERIOD_STEPS)
#define scale_delay_2_us         (((float) 1e6 / scale_freq2) / SINE_PERIOD_STEPS)
#define scale_delay_3_us         (((float) 1e6 / scale_freq3) / SINE_PERIOD_STEPS)
#define scale_delay_4_us         (((float) 1e6 / scale_freq4) / SINE_PERIOD_STEPS)
#define scale_delay_5_us         (((float) 1e6 / scale_freq5) / SINE_PERIOD_STEPS)
#define scale_delay_6_us         (((float) 1e6 / scale_freq6) / SINE_PERIOD_STEPS)
#define scale_delay_7_us         (((float) 1e6 / scale_freq7) / SINE_PERIOD_STEPS)
#define scale_delay_8_us         (((float) 1e6 / scale_freq8) / SINE_PERIOD_STEPS)

// --- seems to be the max possible (3.3v)
#define VOL_ENVELOPE .1

uint16_t sineWave[SINE_PERIOD_STEPS];

char str_ready[] = "ready";
char str_sine[] = "sine: %d %d";

volatile bool play_siren = false;

app_t app;
leds_t *leds = &app.leds;

void DAC0_setVal (uint16_t value) {
  // uint8_t *x = (uint8_t *) 0x06A2;
  // uint8_t *y = x + 1;
  // *x = (value & 0x03) << 6;
  // *y = value >> 2;
  DAC0.DATAL = (value & 0x03) << 6;
  DAC0.DATAH = value >> 2;
}

static void sineWaveInit (void) {
  for (uint8_t i = 0; i < SINE_PERIOD_STEPS; i++) {
    sineWave[i] = VOL_ENVELOPE * (SINE_DC_OFFSET + (float) SINE_AMPLITUDE * sin (2.0 * M_PI * i / SINE_PERIOD_STEPS));
  }
}

void do_scale_note (uint8_t n) {
  static uint8_t idx = 0;
  DAC0_setVal (sineWave[idx]);
  idx = (idx + 1) % SINE_PERIOD_STEPS;
  if (n == 1) _delay_us (scale_delay_1_us);
  else if (n == 2) _delay_us (scale_delay_2_us);
  else if (n == 3) _delay_us (scale_delay_3_us);
  else if (n == 4) _delay_us (scale_delay_4_us);
  else if (n == 5) _delay_us (scale_delay_5_us);
  else if (n == 6) _delay_us (scale_delay_6_us);
  else if (n == 7) _delay_us (scale_delay_7_us);
  else if (n == 8) _delay_us (scale_delay_8_us);
}

void scale_test () {
  uint32_t m = 5000;
  uint32_t n = (float) m * scale_freq2 / scale_freq1;
  uint32_t o = (float) m * scale_freq3 / scale_freq1;
  uint32_t p = (float) m * scale_freq4 / scale_freq1;
  uint32_t q = (float) m * scale_freq5 / scale_freq1;
  uint32_t r = (float) m * scale_freq6 / scale_freq1;
  uint32_t s = (float) m * scale_freq7 / scale_freq1;
  uint32_t t = (float) m * scale_freq8 / scale_freq1;

  while (true) {
    for (uint32_t i = 0; i < m; i++) do_scale_note (1);
    for (uint32_t i = 0; i < n; i++) do_scale_note (2);
    for (uint32_t i = 0; i < o; i++) do_scale_note (3);
    for (uint32_t i = 0; i < p; i++) do_scale_note (4);
    for (uint32_t i = 0; i < q; i++) do_scale_note (5);
    for (uint32_t i = 0; i < r; i++) do_scale_note (6);
    for (uint32_t i = 0; i < s; i++) do_scale_note (7);
    for (uint32_t i = 0; i < t; i++) do_scale_note (8);
  }
}

void do_wave (uint8_t n) {
  static uint8_t idx = 0;
  DAC0_setVal (sineWave[idx]);
  idx = (idx + 1) % SINE_PERIOD_STEPS;
  if (n == 1) _delay_us (delay_1_us);
  else if (n == 2) _delay_us (delay_2_us);
}

int main (void) {
  cli ();
  config_project (&app);
  set_clock_main_internal (F_CPU);
  sineWaveInit ();
  sei ();

  PORTD.PIN6CTRL &= ~PORT_ISC_gm;
  PORTD.PIN6CTRL |= PORT_ISC_INPUT_DISABLE_gc;
  PORTD.PIN6CTRL &= ~PORT_PULLUPEN_bm;

  {
    pinin_t x = mk_pinin (&PORTD, 5, false);
    (void) x;
  }

  {
    pinin_t x = mk_pinin (&PORTD, 7, false);
    (void) x;
  }

  pininctl_set (app.buttons.button1, PORT_PULLUPEN_bm | PORT_ISC_BOTHEDGES_gc);
  pininctl_set (app.buttons.button2, PORT_PULLUPEN_bm | PORT_ISC_BOTHEDGES_gc);
  pininctl_set (app.buttons.button3, PORT_PULLUPEN_bm | PORT_ISC_BOTHEDGES_gc);
  pininctl_set (app.buttons.button4, PORT_PULLUPEN_bm | PORT_ISC_BOTHEDGES_gc);

  VREF.DAC0REF = VREF_REFSEL_2V048_gc | VREF_ALWAYSON_bm;
  _delay_us (VREF_STARTUP_TIME);

  DAC0.CTRLA = DAC_OUTEN_bm | DAC_ENABLE_bm;
  DAC0.CTRLA |= DAC_RUNSTDBY_bm;

  usart_init (&USART1, &PORTC, 0, 1, usart_baud_rate, F_CPU, CLK_PRESCALE);

  for (uint16_t i = 0; i < SINE_PERIOD_STEPS; i++)
    debug_it (str_sine, i, sineWave[i]);

  while (true) {
    if (DO_SCALE_TEST) {
      scale_test ();
      continue;
    }
    if (play_siren) {
      // for (uint8_t i = 0; i < 4; i++) {
      for (uint8_t i = 0; i < 1; i++) {
      uint32_t m = 10000;
      uint32_t n = (float) m * freq2 / freq1;
      for (int i = 0; i < m; i++)
        do_wave (1);
      for (int i = 0; i < n; i++)
        do_wave (2);
      }
    }
    play_siren = false;
  }
  return 1;
}

ISR(BADISR_vect) {
}

ISR (PORTF_PORT_vect) {
  play_siren = pin_is_off (app.buttons.button1);
  PORTF.INTFLAGS = 0xFF;
}

ISR (TCA0_OVF_vect) {
}

ISR (TCB0_INT_vect) {
}
