#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

#include "config-project.h"
#include "config-project-mx-test.h"

#include <util/delay.h>

#include <alleycat-avr.h>

#define dbg(...) do { \
  aca_debug (app.printer, ##__VA_ARGS__); \
} while (0)

#define dbg_no_nl(...) do { \
  aca_debug_no_nl (app.printer, ##__VA_ARGS__); \
} while (0)

#define inf(...) do { \
  aca_info (app.printer, ##__VA_ARGS__); \
} while (0)

#define war(...) do { \
  aca_warn (app.printer, ##__VA_ARGS__); \
} while (0)

#define err(...) do { \
  aca_error (app.printer, ##__VA_ARGS__); \
} while (0)

#define err_no_nl(...) do { \
  error_no_nl (app.printer, ##__VA_ARGS__); \
} while (0)

#define die(...) do { \
  aca_die (app.printer, ##__VA_ARGS__); \
} while (0)

#define try_rf(f, args) do { \
  if (!(f args)) return false; \
} while (0)

#define try_rfe(f, args, fmt, ...) do { \
  if (!(f args)) { \
    char fmt2[255]; \
    snprintf (fmt2, 255, "Error: %s", fmt); \
    err (fmt2, ##__VA_ARGS__); \
    return false; \
  } \
} while (0)

struct led {
  aca_pinout_t led0;
  aca_pinout_t led1;
  aca_pinout_t led2;
  aca_pinout_t led3;
};

struct app {
  aca_printer_t printer;
  struct led leds;
} app;

volatile bool sleeping = false;

uint8_t write_data = 0x42;

void debug_buf (size_t n, uint8_t *buf) {
  uint8_t row_length = 10;
  size_t row = 0;
  while (row * row_length < n) {
    uint8_t len = row_length * 4 + row_length - 1 + 1;
    char row_str[len];
    char *cur = row_str;
    for (uint8_t i = 0; i < row_length; i++) {
      size_t m = row*row_length + i;
      if (m == n) break;
      if (i == 0) {
        snprintf (cur, 5, "0x%02x", buf[m]);
        cur += 4;
      }
      else {
        snprintf (cur, 6, " 0x%02x", buf[m]);
        cur += 5;
      }
    }
    dbg (row_str);
    row += 1;
  }
}

static void init_printer (aca_printer_t *printer, uint8_t prescale) {
  if (!SPEAK) {
    *printer = aca_mk_printer_null ();
    return;
  }
  aca_speak_set_level (SPEAK_LEVEL);
  aca_usart_t usart = aca_usart_init_default (ACA_USART_CONFIG_USART1, USART_BAUD_RATE);
  *printer = aca_mk_printer_usart (usart);
}

static void init_leds (struct led *leds) {
  leds->led0 = aca_mk_pinout (&LED0_PORT, LED0_POS, false);
  leds->led1 = aca_mk_pinout (&LED1_PORT, LED1_POS, false);
  leds->led2 = aca_mk_pinout (&LED2_PORT, LED2_POS, false);
  leds->led3 = aca_mk_pinout (&LED3_PORT, LED3_POS, false);
}

bool mx_test (aca_spi_t spi) {
  uint8_t datan = 10;
  uint8_t buf[datan];
  write_data += 1;
  for (uint8_t i = 0; i < datan; i++)
    buf[i] = write_data + i;

  float clk_rate = (float) F_CPU / CLK_PRESCALE;
  dbg ("mx_test (): using clk_rate=%lu", (uint32_t) clk_rate);
  char id[255];
  try_rfe (aca_mx_read_identification, (spi, 255, id), "read identification");
  inf ("read identification: %s", id);

  uint8_t read[datan];
  try_rfe (aca_mx_read, (spi, 0, datan, read), "initial read");
  inf ("initial read");
  debug_buf (datan, read);

  try_rfe (aca_mx_write_enable_and_check, (spi, clk_rate), "write enable");
  inf ("write enable succeeded");
  bool erase_ok;
  try_rfe (aca_mx_erase_sector_and_check, (spi, 0, clk_rate, &erase_ok), "erase sector");
  if (!erase_ok) {
    err ("aca_mx_erase_sector_and_check (timeout?)");
    return false;
  }
  inf ("erase sector succeeded");
  try_rfe (aca_mx_write_enable_and_check, (spi, clk_rate), "second write enable");
  inf ("second write enable succeeded");
  bool found_srwd;
  char reason[255];
  if (!aca_mx_write_page_and_check (spi, 0, 0, datan, buf, clk_rate, &found_srwd, 255, reason)) {
    err (reason);
    return false;
  }
  inf ("write page succeeded");
  if (found_srwd)
    war ("write page and check: found SRWD bit but continuing");

  dbg ("wrote");
  debug_buf (datan, buf);

  uint8_t read2[datan];
  try_rf (aca_mx_read, (spi, 0, datan, read2));
  inf ("new read succeeded");
  dbg ("read");
  debug_buf (datan, read2);

  return true;
}

int main (void) {
  cli ();
  // "Supports clock frequencies up to 133MHz for all protocols"
  aca_clock_main_internal_set_freq (F_CPU);
  aca_clock_main_set_prescale (CLK_PRESCALE);

  aca_pinout_t enable = aca_mk_pinout (&PORTF, 0, true);
  (void) enable;

  init_printer (&app.printer, CLK_PRESCALE);
  init_leds (&app.leds);
  sei ();

  // set_sleep_mode (SLEEP_MODE_PWR_DOWN);

  aca_spi_t spi = aca_spi_init (
    ACA_SPI_CONFIG_SPI1_ALT1,
    SPI_MASTER_bm,
    SPI_SSD_bm
  );
  if (spi == NULL) return 1;

  _delay_ms (1000.0 / CLK_PRESCALE);

  dbg ("ready");
  dbg ("helloz!!");

  float clk_rate;
  if (!aca_clock_main_get_freq_prescaled (&clk_rate))
    return 1;
  inf ("clk rate (rounded): %lu", (uint32_t) clk_rate);
  if (!mx_test (spi)) err ("mx test failed");
  else inf ("mx test passed!");
  return 1;
}

ISR(BADISR_vect) {
}

ISR(PORTF_PORT_vect) {
  PORTF.INTFLAGS = 0xFF;
}
