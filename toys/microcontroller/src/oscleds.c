#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/atomic.h>

#include "config-project.h"
#include "config-project-oscleds.h"

#include <util/delay.h>

#include <alleycat-avr.h>
#include <alleycat-avr/bits.h>

#include "lib-oscleds.h"

#define dbg(...) do { \
  aca_debug (app.printer, ##__VA_ARGS__); \
} while (0)

#define dbg_no_nl(...) do { \
  aca_debug_no_nl (app.printer, ##__VA_ARGS__); \
} while (0)

#define inf(...) do { \
  aca_info (app.printer, ##__VA_ARGS__); \
} while (0)

#define err(...) do { \
  aca_error (app.printer, ##__VA_ARGS__); \
} while (0)

#define die(...) do { \
  aca_die (app.printer, ##__VA_ARGS__); \
} while (0)

#define try_mk_timer_rf(pref, ...) do { \
  if (aca_timer_mk_timer (__VA_ARGS__) == NULL) { \
    err ("init_timers (): %s: error: %s", pref, reasonn ? reason : "?"); \
    return false; \
  } \
} while (0);

// --- large, because the spurious events when the button is released are
// really hard to deal with otherwise.
#define DEBOUNCE_MS 200
#define MS_PER_TICK 3
#define MS_PER_TICK_PIT 0.979
#define NUM_BUTTONS 4

#define MODE_SINGLE 1
#define MODE_OSCLEDS 2
#define MODE_LEDS_TEST 3

#define MODE MODE_SINGLE

typedef void (*debounce_cb_t) (void *data);

struct leds4 {
  aca_pinout_t led0;
  aca_pinout_t led1;
  aca_pinout_t led2;
  aca_pinout_t led3;
  uint8_t cnt;
};

struct button {
  aca_pinin_t pin;
  bool pressed;
};

struct buttons4 {
  uint8_t cnt;
  struct button button0;
  struct button button1;
  struct button button2;
  struct button button3;
};

struct debounce {
  aca_pinin_t pin;
  bool first;
  uint64_t last_ms;
  uint16_t debounce_ms;
  debounce_cb_t cb;
  void *cb_data;
};

struct debouncer {
  struct debounce debounce0;
  struct debounce debounce1;
  struct debounce debounce2;
  struct debounce debounce3;
};

struct app {
  aca_printer_t printer;
  aca_loop_t loop;
  struct buttons4 buttons;
  // @todo ugly
  uint64_t prev_tick;
  struct leds4 leds;
  struct debouncer debouncer;
  aca_pinout_t *mode_single_cur_led;
} app;

volatile bool sleeping = false;
volatile uint8_t tca_tick = 0;
volatile uint8_t rtc_tick = 0;
volatile uint8_t pit_tick = 0;

void buttons4_clear_pressed (struct buttons4 *b4) {
  b4->button0.pressed = false;
  b4->button1.pressed = false;
  b4->button2.pressed = false;
  b4->button3.pressed = false;
}

uint64_t ticks_to_ms (uint64_t ticks) {
  return ticks * MS_PER_TICK;
}

uint64_t ms_to_tick (uint64_t ms) {
  return ms * 1.0 / MS_PER_TICK;
}

float clk_rate () {
  return (float) F_CPU / CLK_PRESCALE;
}

static void init_printer (aca_printer_t *printer) {
  if (!SPEAK) {
    *printer = aca_mk_printer_null ();
    return;
  }
  aca_speak_set_level (SPEAK_LEVEL);
  aca_usart_t usart = aca_usart_init_default (ACA_USART_CONFIG_USART1, USART_BAUD_RATE);
  *printer = aca_mk_printer_usart (usart);
}

bool init_osc_leds (struct osc_led *osc_leds) {
  if (!osc_leds_init ())
    return false;
  struct leds4 *leds = &app.leds;
  if (!init_osc_led (leds->led0, &osc_leds [0])) return false;
  if (!init_osc_led (leds->led1, &osc_leds [1])) return false;
  if (!init_osc_led (leds->led2, &osc_leds [2])) return false;
  if (!init_osc_led (leds->led3, &osc_leds [3])) return false;
  return true;
}

static void init_app (struct app *app) {
  app->mode_single_cur_led = &app->leds.led0;
}

static bool init_loop (aca_loop_t *loop) {
  uint8_t reasonn = 255;
  char reason[reasonn];
  *loop = aca_loop_mk_loop (8, ACA_TIMER_TCA0_16, MS_PER_TICK, reasonn, reason);
  if (loop == NULL) {
    err ("init_loop (): %s", reason);
    return false;
  }
  return true;
}

static void debounce_init (struct debounce *debounce, struct button *btn, uint16_t debounce_ms, debounce_cb_t cb, void *cb_data) {
  *debounce = (struct debounce) {
    .pin = btn->pin,
    .first = true,
    .last_ms = 0,
    .debounce_ms = debounce_ms,
    .cb = cb,
    .cb_data = cb_data,
  };
}

static void alter_ms (aca_loop_timeout_t timeout, float delta_ms) {
  aca_loop_timeout_alter_ms_delta (timeout, delta_ms);
}

void button (void *data) {
  struct button *btn = (struct button *) data;
  btn->pressed = true;
  dbg ("button pressed");
#if 0
  if (n > NUM_LEDS - 1) return false;
  struct osc_led *osc_led = &osc_leds [n];
  state_next (&osc_led->state);
  osc_led->cycles_remaining = osc_led->state.cycle.speed.max_cycles;
  osc_led->limit_cycles = osc_led->cycles_remaining != 0;
  // timeout_t timeout = &loop.timeouts [n];
  uint8_t num_timeouts;
  aca_loop_timeout_t *ts;
  aca_loop_get_timeouts (app.loop, &num_timeouts, &ts);
  // @todo
  aca_loop_timeout_t timeout = ts[n];
  if (osc_led->state.cycle.speed.on_ms == 0) {
    aca_loop_timeout_pause (timeout);
    aca_pin_off (osc_led->led);
  }
  else {
    aca_loop_timeout_alter_ms (timeout, clk_rate (), aca_loop_get_tick (app.loop), 1);
    aca_loop_timeout_unpause (timeout);
  }
  return true;
#endif
}

void init_debounce (struct debouncer *debouncer, struct buttons4 *buttons) {
  struct button *btn0 = &buttons->button0;
  struct button *btn1 = &buttons->button1;
  struct button *btn2 = &buttons->button2;
  struct button *btn3 = &buttons->button3;
  debounce_init (&debouncer->debounce0, btn0, DEBOUNCE_MS, button, btn0);
  debounce_init (&debouncer->debounce1, btn1, DEBOUNCE_MS, button, btn1);
  debounce_init (&debouncer->debounce2, btn2, DEBOUNCE_MS, button, btn2);
  debounce_init (&debouncer->debounce3, btn3, DEBOUNCE_MS, button, btn3);
}

bool led_single_cb (aca_loop_timeout_t timeout, void *data) {
  aca_pinout_t **pin = (aca_pinout_t **) data;
  aca_pin_toggle (**pin);
  return true;
}

bool led_cb (aca_loop_timeout_t timeout, void *data) {
  struct osc_led *osc_led = (struct osc_led *) data;
  aca_pinout_t led = osc_led->led;
  inf ("blinky tca=%d rtc=%d pit=%d app=%lu", tca_tick, rtc_tick, pit_tick, (uint32_t) aca_loop_get_tick (app.loop));
  struct state *state = &osc_led->state;
  bool onoff = state->onoff;
  if (onoff) aca_pin_on (led);
  else aca_pin_off (led);
  state->onoff = !onoff;
  if (osc_led->limit_cycles) {
    if (osc_led->cycles_remaining) osc_led->cycles_remaining -= 1;
    else {
      // state->cycle = osc_speed_cycles [0];
      state->onoff = false;
      aca_pin_off (led);
      return false;
    }
  }
  return true;
}

bool talk_cb (aca_loop_timeout_t timeout, void *data) {
  // dbg ("talk: %d", pit_tick);
  dbg ("talk");
  return true;
}

bool debounce_check (struct debounce *debounce, uint64_t cur_ms) {
  uint64_t last_ms = debounce->last_ms;
  bool ok;
  if (debounce->first) {
    debounce->first = false;
    ok = true;
  }
  else ok = cur_ms - last_ms >= debounce->debounce_ms;
  if (ok) debounce->last_ms = cur_ms;
  return ok;
}

bool poll_debounce (uint64_t ms, struct debounce *debounce) {
  if (aca_pin_is_on (debounce->pin)) return false;
  if (!debounce_check (debounce, ms))
    return false;
  if (debounce->cb != NULL) debounce->cb (debounce->cb_data);
  return true;
}

static void poll_buttons (aca_loop_t loop, struct debouncer *debouncer) {
  uint64_t ms = ticks_to_ms (aca_loop_get_tick (loop));
  poll_debounce (ms, &debouncer->debounce0);
  poll_debounce (ms, &debouncer->debounce1);
  poll_debounce (ms, &debouncer->debounce2);
  poll_debounce (ms, &debouncer->debounce3);
}

static void init_leds4 (struct leds4 *leds, aca_pinout_t p0, aca_pinout_t p1, aca_pinout_t p2, aca_pinout_t p3) {
  leds->cnt = 4;
  leds->led0 = p0;
  leds->led1 = p1;
  leds->led2 = p2;
  leds->led3 = p3;
}

static void init_leds (struct leds4 *leds) {
  init_leds4 (leds,
    aca_mk_pinout (&LED0_PORT, LED0_POS, false),
    aca_mk_pinout (&LED1_PORT, LED1_POS, false),
    aca_mk_pinout (&LED2_PORT, LED2_POS, false),
    aca_mk_pinout (&LED3_PORT, LED3_POS, false)
  );
}

static void init_buttons4 (struct buttons4 *b4, aca_pinin_t p0, aca_pinin_t p1, aca_pinin_t p2, aca_pinin_t p3) {
  *b4 = (struct buttons4) {
    .cnt = 4,
    .button0 = {
      .pin = p0,
      .pressed = false,
    },
    .button1 = {
      .pin = p1,
      .pressed = false,
    },
    .button2 = {
      .pin = p2,
      .pressed = false,
    },
    .button3 = {
      .pin = p3,
      .pressed = false,
    },
  };
}

static void init_inputs (struct buttons4 *buttons) {
  aca_pinin_t p0 = aca_mk_pinin (&BTN0_PORT, BTN0_POS, true);
  aca_pinin_t p1 = aca_mk_pinin (&BTN1_PORT, BTN1_POS, true);
  aca_pinin_t p2 = aca_mk_pinin (&BTN2_PORT, BTN2_POS, true);
  aca_pinin_t p3 = aca_mk_pinin (&BTN3_PORT, BTN3_POS, true);
  init_buttons4 (buttons, p0, p1, p2, p3);
  write (aca_pinin_ctl (p0), PORT_ISC_BOTHEDGES_gc, 0x7);
  write (aca_pinin_ctl (p1), PORT_ISC_BOTHEDGES_gc, 0x7);
  write (aca_pinin_ctl (p2), PORT_ISC_BOTHEDGES_gc, 0x7);
  write (aca_pinin_ctl (p3), PORT_ISC_BOTHEDGES_gc, 0x7);
}

static bool init_extra_timers (float ms_per_tick, float ms_per_tick_pit) {
  uint8_t reasonn = 255;
  char reason[reasonn];
aca_timer_set_printer (app.printer);
  try_mk_timer_rf ("rtc rtc", ACA_TIMER_RTC_RTC, ms_per_tick, reasonn, reason);
  // --- acceptable values @ 32.768k:
  // 1000, 500, 250, ..., 0.977 (2**5 / 32.768), ... 0.122 (2**2 / 32.768)
  try_mk_timer_rf ("rtc pit", ACA_TIMER_RTC_PIT, ms_per_tick_pit, reasonn, reason);
  return true;
}

static volatile void timer_tick () {
  aca_loop_tick (app.loop);
}

void doit2 (struct osc_led *osc_led) {
}

void doit (aca_loop_timeout_t timeout, struct osc_led *osc_led) {
}

void mode_single_cycle_cur_led () {
  if (app.mode_single_cur_led == &app.leds.led0)
    app.mode_single_cur_led = &app.leds.led1;
  else if (app.mode_single_cur_led == &app.leds.led1)
    app.mode_single_cur_led = &app.leds.led2;
  else if (app.mode_single_cur_led == &app.leds.led2)
    app.mode_single_cur_led = &app.leds.led3;
  else if (app.mode_single_cur_led == &app.leds.led3)
    app.mode_single_cur_led = &app.leds.led0;
  else return;
}

int main (void) {
  cli ();
  aca_clock_main_internal_set_freq (F_CPU);
  aca_clock_main_set_prescale (CLK_PRESCALE);

  // --- give clock a moment to settle before we init UART
  if (SPEAK) aca_delay_ms_basic (100);

  init_app (&app);
  init_inputs (&app.buttons);
  init_debounce (&app.debouncer, &app.buttons);
  if (!init_loop (&app.loop))
    return 1;
  if (!init_extra_timers (MS_PER_TICK, MS_PER_TICK_PIT))
    return 1;

  init_leds (&app.leds);
  init_printer (&app.printer);

  struct leds4 *leds = &app.leds;
  struct osc_led osc_leds [leds->cnt];
  init_osc_leds (osc_leds);

  sei ();

  inf ("ready");

  aca_loop_timeout_t timeout[NUM_OUTS];
  if (MODE == MODE_SINGLE) {
    if (!aca_loop_timeout_add (app.loop, led_single_cb, (void *) &app.mode_single_cur_led, 1000, &timeout [0])) {
      err ("aca_loop_add_timeout failed");
      return 1;
    }
  }
  else if (MODE == MODE_OSCLEDS) {
    // todo n
    for (uint8_t i = 0; i < NUM_OUTS; i++) {
      struct osc_led *led = &osc_leds [i];
      // todo store
      if (!aca_loop_timeout_add (app.loop, led_cb, (void *) led, osc_led_cur_on_ms (led), &timeout [i])) {
        err ("aca_loop_add_timeout failed");
        return 1;
      }
    }
  }
  else if (MODE == MODE_LEDS_TEST)
    while (true) {
      aca_pin_toggle (leds->led0);
      aca_pin_toggle (leds->led1);
      aca_pin_toggle (leds->led2);
      aca_pin_toggle (leds->led3);
      _delay_ms (500);
      aca_pin_toggle (leds->led0);
      aca_pin_toggle (leds->led1);
      aca_pin_toggle (leds->led2);
      aca_pin_toggle (leds->led3);
      _delay_ms (500);
    }

  else return 1;

  set_sleep_mode (SLEEP_MODE_PWR_DOWN);
  uint64_t prev = 0;
  struct buttons4 *buttons = &app.buttons;
  while (true) {
    uint64_t cur = aca_loop_get_tick (app.loop);
    // --- timer hasn't ticked again yet
    if (prev == cur) continue;
    prev = cur;
    poll_buttons (app.loop, &app.debouncer);

#if MODE == MODE_SINGLE
    if (buttons->button0.pressed) alter_ms (timeout [0], 100);
    if (buttons->button1.pressed) alter_ms (timeout [0], -100);
    if (buttons->button2.pressed) mode_single_cycle_cur_led ();
#elif MODE == MODE_OSCLEDS
    if (buttons->button0.pressed) doit (timeout [0], &osc_leds [0]);
#else
    else return 1;
#endif

#if 0
  struct osc_speed *speed = &state->cycle.speed;
  uint16_t ms = onoff ? speed->off_ms : speed->on_ms;
  aca_loop_timeout_alter_ms (timeout, ms);
  aca_loop_timeout_alter_cb_data (timeout, (void *) osc_led);
#endif

    buttons4_clear_pressed (&app.buttons);

    aca_loop_do_timeouts (app.loop);

#if 0
    if (!aca_loop_has_active_timeout (app.loop)) {
      // --- because the ticks get paused, we need to clear the values for
      // `last_tick` in the debounce state, or else the buttons will
      // fail for a very long time.
      init_debounce (&app.debouncer, &app.buttons);
      dbg ("sleeping");
      sleep_mode ();
      // --- from docs, do this upon waking up.
      ATOMIC_BLOCK (ATOMIC_FORCEON) _delay_ms (10);
      dbg ("woke up");
    }
#endif
  }

  return 1;
}

ISR (BADISR_vect) {
}

ISR (PORTF_PORT_vect) {
  PORTF.INTFLAGS = 0xFF;
}

#if 0
ISR (ACA_TIMER_TCA0_8_0_VECTOR) {
  if (ACA_TIMER_TCA0_8_0_INTR_CONDITION) {
    timer_tick (MS_PER_TICK);
    ACA_TIMER_TCA0_8_0_INTR_CLEAR ();
  }
}

ISR (ACA_TIMER_TCA0_8_1_VECTOR) {
  if (ACA_TIMER_TCA0_8_1_INTR_CONDITION) {
    timer_tick (MS_PER_TICK);
    ACA_TIMER_TCA0_8_1_INTR_CLEAR ();
  }
}

ISR (ACA_TIMER_TCA1_16_VECTOR) {
  if (ACA_TIMER_TCA1_16_INTR_CONDITION) {
    timer_tick (MS_PER_TICK);
    ACA_TIMER_TCA1_16_INTR_CLEAR ();
  }
}

ISR (ACA_TIMER_TCA1_8_0_VECTOR) {
  if (ACA_TIMER_TCA1_8_0_INTR_CONDITION) {
    timer_tick (MS_PER_TICK);
    ACA_TIMER_TCA1_8_0_INTR_CLEAR ();
  }
}

ISR (ACA_TIMER_TCA1_8_1_VECTOR) {
  if (ACA_TIMER_TCA1_8_1_INTR_CONDITION) {
    timer_tick (MS_PER_TICK);
    ACA_TIMER_TCA1_8_1_INTR_CLEAR ();
  }
}

ISR (ACA_TIMER_TCB0_VECTOR) {
  if (ACA_TIMER_TCB0_INTR_CONDITION) {
    timer_tick (MS_PER_TICK);
  }
  ACA_TIMER_TCB0_INTR_CLEAR ();
}

ISR (ACA_TIMER_TCB1_VECTOR) {
  if (ACA_TIMER_TCB1_INTR_CONDITION) {
    timer_tick (MS_PER_TICK);
  }
  ACA_TIMER_TCB1_INTR_CLEAR ();
}

ISR (ACA_TIMER_TCB2_VECTOR) {
  if (ACA_TIMER_TCB2_INTR_CONDITION) {
    timer_tick (MS_PER_TICK);
  }
  ACA_TIMER_TCB2_INTR_CLEAR ();
}

ISR (ACA_TIMER_TCB3_VECTOR) {
  if (ACA_TIMER_TCB3_INTR_CONDITION) {
    timer_tick (MS_PER_TICK);
  }
  ACA_TIMER_TCB3_INTR_CLEAR ();
}

#endif

ISR (ACA_TIMER_TCA0_16_VECTOR) {
  if (ACA_TIMER_TCA0_16_INTR_CONDITION) {
    tca_tick++;
    timer_tick ();
    ACA_TIMER_TCA0_16_INTR_CLEAR ();
  }
}

ISR (ACA_TIMER_RTC_RTC_VECTOR) {
  if (ACA_TIMER_RTC_RTC_INTR_CONDITION) {
    rtc_tick++;
    // timer_tick ();
    ACA_TIMER_RTC_RTC_INTR_CLEAR ();
  }
}

ISR (ACA_TIMER_RTC_PIT_VECTOR) {
  if (ACA_TIMER_RTC_PIT_INTR_CONDITION) {
    pit_tick++;
    // timer_tick ();
    ACA_TIMER_RTC_PIT_INTR_CLEAR ();
  }
}
